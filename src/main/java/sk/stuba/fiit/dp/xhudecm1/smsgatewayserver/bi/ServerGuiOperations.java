/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.compression.CompressionHandlerServer;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.KeyExchangeNotFinishedYetException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToDecryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToGenerateKeyPairs;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToRetrieveDataFromDb;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UserDbOpertaionException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.GenerateCustomAlphabet;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.MessageDetail;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.RegisterNewDevice;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.SendTestMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationDataHistory;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.MobileNumbers;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.RegisteredUser;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUKey;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUTechnical;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;

/**
 *
 * @author martinhudec
 */
public class ServerGuiOperations {

    private static DbHandler dbHandler;
    private static final CompressionHandlerServer compressionHandler = CompressionHandlerServer.getInstance();
    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ServerGuiOperations.class);

    BusinessLogicHandler biHandler;

    public ServerGuiOperations(BusinessLogicHandler biHandler) {
        dbHandler= DbHandler.getInstance();
        this.biHandler = biHandler;
    }

    public void createNewUser(String email) throws UserDbOpertaionException {
        dbHandler.dbWriteRegisteredUser(email, email);
    }

    public void registerNewDeviceForUser(RegisterNewDevice regNewDevRequest) throws UserDbOpertaionException {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            RegisteredUser regUser = dbHandler.dbReadUser(regNewDevRequest.getUserEmail());
            MobileNumbers mobileNumber = (MobileNumbers) session.get(MobileNumbers.class, regNewDevRequest.getMobileNumber());
            LOG.debug("Registering new device for request " + regNewDevRequest.toString());
            dbHandler.dbWriteClientSecureCommunicationData(regUser, mobileNumber, session, regNewDevRequest.getDeviceName(), regNewDevRequest.getDescription());
            tx.commit();
        } catch (HibernateException | UnableToRetrieveDataFromDb | UnableToGenerateKeyPairs ex) {
            LOG.error("exception while registering new device for request " + regNewDevRequest);
            tx.rollback();
            throw new UserDbOpertaionException("exception while registering new device for request", ex);
        } finally {
            session.close();
        }

    }

    public void generateCustomAlphabet(GenerateCustomAlphabet genCustAlphRequest) throws UserDbOpertaionException {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            ClientSecureCommunicationData secureData = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, genCustAlphRequest.getDeviceId());

            String dataSetAlphabet = compressionHandler.generateCustomAlphabetJson(genCustAlphRequest.getDataSet());
            LOG.error(dataSetAlphabet);
            secureData.setCustomAlphabet(dataSetAlphabet);
            secureData.setModifiedAt(new Date());
            session.save(secureData);
            tx.commit();
        } catch (HibernateException ex) {
            LOG.error("exception while generating custom alphabet for request " + genCustAlphRequest);
            tx.rollback();
            throw new UserDbOpertaionException("exception while generating custom alphabet for request", ex);
        } finally {
            session.close();
        }
    }

    public void sendTestMessage(SendTestMessage sendTestMessage) throws UserDbOpertaionException {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            ClientSecureCommunicationData secureData = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, sendTestMessage.getDeviceId());
            LOG.debug("Sending test message PING " + sendTestMessage.toString() );
            biHandler.sendTechnicalPingMessage(secureData);
        } catch (HibernateException ex) {
            LOG.error("exception while sending test message for request " + sendTestMessage);
            throw new UserDbOpertaionException("exception while sending test message", ex);
        } finally {
            tx.commit();
            session.close();
        }
    }

    public void sendKeyExchangeRequest(SendTestMessage sendTestMessage) throws UserDbOpertaionException {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            ClientSecureCommunicationData secureData = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, sendTestMessage.getDeviceId());

            biHandler.sendTechnicalKeyExchangeRequestMessage(secureData);
        } catch (HibernateException ex) {
            LOG.error("exception while sending key exchange request message for request " + sendTestMessage);
            throw new UserDbOpertaionException("exception while sending key exchange request message", ex);
        } finally {
            session.close();
        }
    }

    public String getServerWsdl() {
        return biHandler.getWsHandler().getWsdl();
    }

    public MessageDetail getMessageDetail(String messageId) throws UserDbOpertaionException {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            SmsReceived received = (SmsReceived) session.get(SmsReceived.class, Long.parseLong(messageId));
            SmsSecurePDU securePdu = SmsSecurePDU.createNewSecurePdu(Utils.toByteArray(received.getHexPayload()));
            LOG.debug("Getting detail for message " + messageId);
            switch (securePdu.getContentType()) {
                case DATA:
                    SmsSecurePDUData securePduData = (SmsSecurePDUData) securePdu;
                    if (securePduData.getCypherActive().equals(SmsSecurePDU.CypherActiveEnum.OFF)){
                        securePduData.setDecompressedPayload(securePduData.getPayload());
                        securePduData.setDecryptedPayload(securePduData.getPayload());
                    }else { 
                        if (received.getTimestamp().after(received.getClientSecureCommunicationData().getAesKeyModifiedAt())) {
                            securePduData.setDecryptedPayload(biHandler.decryptData(securePduData.getPayload(), received.getClientSecureCommunicationData()));
                            securePduData.setDecompressedPayload(compressionHandler.decompressData(securePduData.getDecryptedPayload(), securePduData.getCompressionType(), compressionHandler.loadCustomAlphabetFromJson(received.getClientSecureCommunicationData().getCustomAlphabet())));
                        }else { 
                            List<ClientSecureCommunicationDataHistory> secureDataHistoryList = session.createCriteria(ClientSecureCommunicationDataHistory.class).add(Restrictions.ge("usedTo", received.getTimestamp())).add(Restrictions.le("usedForm", received.getTimestamp())).list();
                            if (secureDataHistoryList.isEmpty()){
                                throw new UserDbOpertaionException("no secure data history for message decryption message detail message id " + messageId);
                            }
                            ClientSecureCommunicationDataHistory secureDataHistory = secureDataHistoryList.get(0);
                            LOG.debug(secureDataHistory.getId());
                            securePduData.setDecryptedPayload(biHandler.decryptData(securePduData.getPayload(), secureDataHistory));
                            LOG.debug(new String(securePduData.getDecryptedPayload()));
                            securePduData.setDecompressedPayload(compressionHandler.decompressData(securePduData.getDecryptedPayload(), securePduData.getCompressionType(), compressionHandler.loadCustomAlphabetFromJson(received.getClientSecureCommunicationData().getCustomAlphabet())));
                        }
                    }
                    
                    LOG.debug(new String(securePduData.getPureData()));
                    MessageDetail messageDetail = new MessageDetail(new String(securePduData.getPureData()),securePduData.getCompressionType().toString(), securePduData.getContentType().toString(), securePduData.getDestinationType().toString());
                    return messageDetail;
                    
                case KEY:
                    LOG.debug("KEY");
                    SmsSecurePDUKey securePduKey = (SmsSecurePDUKey) securePdu;
                    messageDetail = new MessageDetail("", "",securePduKey.getContentType().toString(), "");
                    return messageDetail;
                    
                case TECHNICAL:
                    LOG.debug("TECHNICAL");
                    SmsSecurePDUTechnical securePDUTechnical = (SmsSecurePDUTechnical) securePdu;
                    messageDetail = new MessageDetail("", securePDUTechnical.getCompressionType().toString(),securePDUTechnical.getContentType().toString(), "");
                    return messageDetail;
            }
        } catch (HibernateException | UknownPduConentType | KeyExchangeNotFinishedYetException | UnableToEncryptDataException | UnableToRetrieveDataFromDb | UnableToDecryptDataException | IOException ex) {
            LOG.error("exception while getting message detail " + messageId);
            throw new UserDbOpertaionException("exception while getting message detail", ex);
        } finally {
            session.close();
        }
        return null;
    }

    public MessageDetail getSentMessageDetail(String messageId) throws UserDbOpertaionException {
               Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            SmsSent sent = (SmsSent) session.get(SmsSent.class, Long.parseLong(messageId));
            SmsSecurePDU securePdu = SmsSecurePDU.createNewSecurePdu(Utils.toByteArray(sent.getPayload()));
            LOG.debug("Getting detail for sent message " + messageId);
            switch (securePdu.getContentType()) {
                case DATA:
                    SmsSecurePDUData securePduData = (SmsSecurePDUData) securePdu;
                    if (sent.getTimestamp().after(sent.getClientSecureCommunicationData().getAesKeyModifiedAt())) {
                        securePduData.setDecryptedPayload(biHandler.decryptData(securePduData.getPayload(), sent.getClientSecureCommunicationData()));
                        securePduData.setDecompressedPayload(compressionHandler.decompressData(securePduData.getDecryptedPayload(), securePduData.getCompressionType(), compressionHandler.loadCustomAlphabetFromJson(sent.getClientSecureCommunicationData().getCustomAlphabet())));
                    }else { 
                        List<ClientSecureCommunicationDataHistory> secureDataHistoryList = session.createCriteria(ClientSecureCommunicationDataHistory.class).add(Restrictions.ge("usedTo", sent.getTimestamp())).add(Restrictions.le("usedForm", sent.getTimestamp())).list();
                        if (secureDataHistoryList.isEmpty()){
                            throw new UserDbOpertaionException("no secure data history for message decryption message detail message id " + messageId);
                        }
                        ClientSecureCommunicationDataHistory secureDataHistory = secureDataHistoryList.get(0);
                        LOG.debug(secureDataHistory.getId());
                        securePduData.setDecryptedPayload(biHandler.decryptData(securePduData.getPayload(), secureDataHistory));
                        LOG.debug(new String(securePduData.getDecryptedPayload()));
                        securePduData.setDecompressedPayload(compressionHandler.decompressData(securePduData.getDecryptedPayload(), securePduData.getCompressionType(), compressionHandler.loadCustomAlphabetFromJson(sent.getClientSecureCommunicationData().getCustomAlphabet())));
                    }
                    
                    LOG.debug(new String(securePduData.getPureData()));
                    MessageDetail messageDetail = new MessageDetail(new String(securePduData.getPureData()),securePduData.getCompressionType().toString(), securePduData.getContentType().toString(), securePduData.getDestinationType().toString());
                    return messageDetail;
                    
                case KEY:
                    LOG.debug("KEY");
                    SmsSecurePDUKey securePduKey = (SmsSecurePDUKey) securePdu;
                    messageDetail = new MessageDetail("", "",securePduKey.getContentType().toString(), "");
                    return messageDetail;
                    
                case TECHNICAL:
                    LOG.debug("TECHNICAL");
                    SmsSecurePDUTechnical securePDUTechnical = (SmsSecurePDUTechnical) securePdu;
                    messageDetail = new MessageDetail("", securePDUTechnical.getCompressionType().toString(),securePDUTechnical.getContentType().toString(), "");
                    return messageDetail;
            }
        } catch (HibernateException | UknownPduConentType | KeyExchangeNotFinishedYetException | UnableToEncryptDataException | UnableToRetrieveDataFromDb | UnableToDecryptDataException | IOException ex) {
            LOG.error("exception while getting message detail " + messageId);
            throw new UserDbOpertaionException("exception while getting message detail", ex);
        } finally {
            session.close();
        }
        return null;
    }

   
}
