/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.MessageSentResultCallback;

/**
 *
 * @author martinhudec
 */
public interface WebServiceProcessedCallback {

    public void WebServiceMessageSentResult(MessageSentResultCallback callback, String wsId);

}
