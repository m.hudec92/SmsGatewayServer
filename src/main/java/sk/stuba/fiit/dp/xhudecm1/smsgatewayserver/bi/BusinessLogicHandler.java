/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.compression.CompressionHandlerServer;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.cyphering.CypheringCurve;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.cyphering.ServerKeyHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums.EmailReceivedStatusTypeEnum;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums.SmsTechnicalStatusTypeEnum;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.EmailNotFoundInInputForwardMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.ErrorProcessingNewEmail;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.ErrorProcessingNewWs;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.KeyExchangeNotFinishedYetException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.KeyExchangeSeqenceNumbersDoNotMatchException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.OptionPropertiesException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToDecryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToGenerateMD5HashException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToGenerateSharedSecret;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToGetServerKeysFromStorage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToRetrieveDataFromDb;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.WsNotFoundInInputForwardMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.DeliverEmailMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.EmailHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws.DeliverWsMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws.WebServiceProcessedCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws.WsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.smshandler.DeliveryStatusMessageReceivedCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.smshandler.DeliveryStatusSecureMessageCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.smshandler.ServerSmsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.BiMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationDataHistory;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailForwardInSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.GatewayModems;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.MobileNumbers;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.RegisteredUser;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsTechnical;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.UnhandledSmsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsForwardInSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsForwardOutSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.KeyNegotiationStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.SmsReceivedStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.SmsSentStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliveryStatusSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.MessageSentResultCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUKey;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUTechnical;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;

/**
 *
 * @author martinhudec
 */
public class BusinessLogicHandler {

    private final DbHandler dbHandler;
    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(BusinessLogicHandler.class);

    private final CypheringCurve cypheringCurve = CypheringCurve.getInstance();
    private final ServerSmsHandler smsHandler;
    private final CompressionHandlerServer compressionHandler = CompressionHandlerServer.getInstance();
    private final EmailHandler emailHandler;
    private final WsHandler wsHandler;
    private final List<DeliveryStatusMessageReceivedCallback> deliveryStatusCallbackList = new CopyOnWriteArrayList<>();
    private final List<WebServiceProcessedCallback> wsMessageProcessedCallbackList = new CopyOnWriteArrayList<>();
    private final ServerKeyHandler keyHandler;
    private final ServerGuiOperations serverGuiOpsHandler;

    public BusinessLogicHandler(ServerSmsHandler smsHandler) throws UnableToGetServerKeysFromStorage {
        this.dbHandler = DbHandler.getInstance();
        this.keyHandler = ServerKeyHandler.getInstance();
        this.smsHandler = smsHandler;
        this.emailHandler = new EmailHandler(this);
        this.wsHandler = new WsHandler(this);
        this.serverGuiOpsHandler = new ServerGuiOperations(this);
    }

    private void processBussinessLogic(DeliverSmsPDU receivedSMSPdu, BiMappings bimapping) throws UnableToRetrieveDataFromDb, UknownPduConentType {

        try {
            dbHandler.dbUpdateReceivedSmsStatus(receivedSMSPdu.getMessageUUID(), SmsReceivedStatusType.SMS_RECEIVED_PROCESSING);

            LOG.debug("SECURE PDU " + receivedSMSPdu.getSecurePdu().toString());
            switch (receivedSMSPdu.getSecurePdu().getContentType()) {
                case DATA:
                    processReceivedDataMessage(receivedSMSPdu, bimapping);
                    break;
                case TECHNICAL:
                    processReceivedTechnicalMessage(receivedSMSPdu, bimapping);
                    break;
            }
            dbHandler.dbUpdateReceivedSmsStatus(receivedSMSPdu.getMessageUUID(), SmsReceivedStatusType.SMS_RECEIVED_PROCESSED);
        } catch (UnableToRetrieveDataFromDb ex) {
            LOG.error("Unable to change received message status ", ex);
            dbHandler.dbUpdateReceivedSmsStatus(receivedSMSPdu.getMessageUUID(), SmsReceivedStatusType.SMS_RECEIVED_ERROR);
        }
    }

    private void processReceivedDataMessage(DeliverSmsPDU receivedSmsPdu, BiMappings bimapping) throws UknownPduConentType {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {

            LOG.debug(receivedSmsPdu.getMessageUUID());
            LOG.debug("RECEIVED MESSAGE PROCESSING AS DATA "  + receivedSmsPdu.getSecurePdu().toString());
            // SmsSecurePDUData receivedDataPdu = (SmsSecurePDUData) receivedSmsPdu.getSecurePdu();
            SmsReceived receivedMessageDao = dbHandler.dbReadSmsReceived(receivedSmsPdu.getMessageUUID(), session);
            ClientSecureCommunicationData secureData = receivedMessageDao.getClientSecureCommunicationData();

            // ak nebolo nastavene sifrovanie tak pouzijeme cisty payload 
            byte[] decrypted = receivedSmsPdu.getSecurePdu().getPayload();
            if (receivedSmsPdu.getSecurePdu().getCypherActive().equals(SmsSecurePDU.CypherActiveEnum.ON)) {
                LOG.debug("DECRYPTING WITH AES " + secureData.getAesKey());
                LOG.debug("DECRYPTING WITH API " + secureData.getApiKey());
                decrypted = decryptData(receivedSmsPdu.getSecurePdu().getPayload(), secureData.getId());
            }
            
            receivedSmsPdu.getSecurePdu().setDecryptedPayload(decrypted);
            //LOG.debug("DECRYPTED DATA " + Utils.toHexString(decrypted));
            // v pripade ze je nastavena nejaka kompresia tak potrebujeme najskor dekomprimovat prijate data 
            if (((SmsSecurePDUData) receivedSmsPdu.getSecurePdu()).getCompressionType() != SmsSecurePDU.CompressionTypeEnum.NONE) {
                ((SmsSecurePDUData) receivedSmsPdu.getSecurePdu()).setDecompressedPayload(
                        compressionHandler.decompressData(
                                decrypted,
                                ((SmsSecurePDUData) receivedSmsPdu.getSecurePdu()).getCompressionType(), compressionHandler.loadCustomAlphabetFromJson(secureData.getCustomAlphabet())));
            }

            LOG.debug("SUCCESFULLY DECRYPTED DATA RECEIVED IN MESSAGE" + new String(((SmsSecurePDUData) receivedSmsPdu.getSecurePdu()).getPureData()));
           
            SmsSecurePDU.DestinationTypeEnum destinationType = SmsSecurePDU.DestinationTypeEnum.valueOf(bimapping.getBiOperationType());

            if (((SmsSecurePDUData) receivedSmsPdu.getSecurePdu()).getDestinationType() != SmsSecurePDU.DestinationTypeEnum.DEFAULT) {
                destinationType = ((SmsSecurePDUData) receivedSmsPdu.getSecurePdu()).getDestinationType();
            }
            LOG.debug("SENDING RECEIVED DATA MESSAGE OVER " + destinationType);
            switch (destinationType) {
                case EMAIL:
                    emailHandler.sendEmail(receivedSmsPdu, secureData.getEmailForwardOutSettingses());
                    break;
                case STORE:
                    LOG.info("messages are stored by default");
                    break;
                case WS:
                    wsHandler.forwardMessageViaWs(receivedSmsPdu, secureData.getWsForwardOutSettingses());
                    break;
            }

        } catch (UnableToRetrieveDataFromDb | UnableToDecryptDataException | KeyExchangeNotFinishedYetException | UnableToEncryptDataException | IOException ex) {
            LOG.error("ERROR while processing received data message " + receivedSmsPdu.toString(), ex);
        } catch (HibernateException ex) {
            LOG.error("ERROR while processing received data message " + receivedSmsPdu.toString(), ex);
            tx.rollback();
        } finally {
            session.close();
        }
    }

    private void processReceivedKeyMessage(DeliverSmsPDU keySmsPdu) throws UnableToRetrieveDataFromDb, UknownPduConentType {
        LOG.info("RECEIVED KEY EXCHANGE MESSAGE");
        try {
            switch (((SmsSecurePDUKey) keySmsPdu.getSecurePdu()).getKeyExchangeStage()) {
                case REQUEST:
                    processReceivedKeyExchangeRequest(keySmsPdu);
                    break;
                case RESPONSE:
                    throw new UnsupportedOperationException("this type of message is not supported yet " + ((SmsSecurePDUKey) keySmsPdu.getSecurePdu()).getKeyExchangeStage());
                case ACK:
                    processReceivedKeyExchangeAck(keySmsPdu);
                    break;
                case API_KEY_REQUEST:
                    processReceivedApiKeyRequest(keySmsPdu);
                    break;
            }
        } catch (UnableToRetrieveDataFromDb | KeyExchangeNotFinishedYetException | KeyExchangeSeqenceNumbersDoNotMatchException | UnableToEncryptDataException | UnableToDecryptDataException | UnableToGenerateMD5HashException | UnableToGenerateSharedSecret ex) {
            dbHandler.dbUpdateReceivedSmsStatus(keySmsPdu.getMessageUUID(), SmsReceivedStatusType.SMS_RECEIVED_ERROR);
            LOG.error("Error processing received key message ", ex);
        }
    }

    private void processReceivedTechnicalMessage(DeliverSmsPDU receivedSMSPdu, BiMappings bimapping) throws UknownPduConentType {

        SmsSecurePDUTechnical secureTechnicalPdu = (SmsSecurePDUTechnical) receivedSMSPdu.getSecurePdu();
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            byte[] decrypted = decryptData(secureTechnicalPdu.getPayload(), bimapping.getClientSecureCommunicationData().getId());
            secureTechnicalPdu.setDecryptedPayload(decrypted);
            //LOG.debug("DECRYPTED DATA " + Utils.toHexString(decrypted));
            // v pripade ze je nastavena nejaka kompresia tak potrebujeme najskor dekomprimovat prijate data 
            if (secureTechnicalPdu.getCompressionType() != SmsSecurePDU.CompressionTypeEnum.NONE) {
                secureTechnicalPdu.setDecompressedPayload(
                        compressionHandler.decompressData(
                                decrypted,
                                secureTechnicalPdu.getCompressionType(), compressionHandler.loadCustomAlphabetFromJson(bimapping.getClientSecureCommunicationData().getCustomAlphabet())));
            }

            List<SmsTechnical> technicalList = session.createCriteria(SmsTechnical.class).add(Restrictions.eq("messageUuid", new String(secureTechnicalPdu.getPureData()))).list();
            if (technicalList.isEmpty()) {
                LOG.error("TECHNICAL MESSAGE recevied has no matching record in DB");
                return;
            }
            SmsTechnical technical = technicalList.get(0);
            technical.setTechnicalMessageStatusType(SmsTechnicalStatusTypeEnum.SERVER_CLIENT_TECHNICAL_ACK_RECEIVED.toString());
            session.save(technical);
            tx.commit();
        } catch (HibernateException ex) {
            LOG.error("Error while processing received technical message " + receivedSMSPdu.toString(), ex);
            tx.rollback();
        } catch (KeyExchangeNotFinishedYetException | IOException | UnableToEncryptDataException | UnableToRetrieveDataFromDb | UnableToDecryptDataException ex) {
            LOG.error("Error while processing received technical message " + receivedSMSPdu.toString(), ex);
        } finally {
            session.close();
        }

        // najst o ktory ping sa jedna najst ho v databaze nastavit mu stav zavolat ajax ? 
    }

    public void sendTechnicalPingMessage(ClientSecureCommunicationData secureData) {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            SmsTechnical technical = new SmsTechnical(
                    secureData.getMobileNumbers(),
                    SmsSecurePDU.TechnicalMessageTypeEnum.PING.toString(),
                    SmsTechnicalStatusTypeEnum.SERVER_CLIENT_TECHNICAL_SENT.toString(),
                    new Date(),
                    new Date(),
                    UUID.randomUUID().toString());

            session.save(technical);
            smsHandler.sendTechnicalSecureMessage(SmsSecurePDU.TechnicalMessageTypeEnum.PING,
                    technical.getMessageUuid().getBytes(),
                    secureData.getMobileNumbers().getMobileNumber(),
                    secureData.getId());
            tx.commit();

        } catch (HibernateException ex) {
            LOG.error("Error while sending technical message to number " + secureData.getMobileNumbers().getMobileNumber(), ex);
            tx.rollback();
        } catch (UnableToRetrieveDataFromDb | KeyExchangeNotFinishedYetException | UnableToEncryptDataException ex) {
            LOG.error("Error while sending technical message to number " + secureData.getMobileNumbers().getMobileNumber(), ex);
        } finally {
            session.close();
        }
    }

    public void sendTechnicalKeyExchangeRequestMessage(ClientSecureCommunicationData secureData) {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        LOG.info("[PHASE 1] sending key exchange REQUEST MESSAGE");
        try {
            SmsTechnical technical = new SmsTechnical(
                    secureData.getMobileNumbers(),
                    SmsSecurePDU.TechnicalMessageTypeEnum.REQUEST_KEY_EXCHANGE.toString(),
                    SmsTechnicalStatusTypeEnum.SERVER_CLIENT_TECHNICAL_SENT.toString(),
                    new Date(),
                    new Date(),
                    UUID.randomUUID().toString());

            session.save(technical);
            smsHandler.sendTechnicalSecureMessage(SmsSecurePDU.TechnicalMessageTypeEnum.REQUEST_KEY_EXCHANGE,
                    cypheringCurve.getMD5Hash(Utils.toByteArray(secureData.getAesKey())),
                    secureData.getMobileNumbers().getMobileNumber(),
                    secureData.getId());
            tx.commit();

        } catch (HibernateException ex) {
            LOG.error("Error while sending technical message to number " + secureData.getMobileNumbers().getMobileNumber(), ex);
            tx.rollback();
        } catch (UnableToRetrieveDataFromDb | KeyExchangeNotFinishedYetException | UnableToEncryptDataException | UnableToGenerateMD5HashException ex) {
            LOG.error("Error while sending technical message to number " + secureData.getMobileNumbers().getMobileNumber(), ex);
        } finally {
            session.close();
        }
    }

    public void processReceivedEmailMessage(DeliverEmailMessage receivedEmail) throws ErrorProcessingNewEmail, EmailNotFoundInInputForwardMappings {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            LOG.debug("processing received email message " + receivedEmail.getMessage().toPrettyString());
        } catch (IOException ex) {
            Logger.getLogger(BusinessLogicHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            List<EmailForwardInSettings> forwardInList = session.createCriteria(EmailForwardInSettings.class)
                    .add(Restrictions.eq("sourceEmailAddress", receivedEmail.getSourceMailAddress()))
                    .add(Restrictions.eq("emailIdentification", receivedEmail.getEmailIdentificationKey()))
                    .add(Restrictions.eq("activeStatus", "ACTIVE"))
                    .list();
            if (forwardInList.isEmpty()) {
                throw new EmailNotFoundInInputForwardMappings("Sender email address with identification id not found in DB, cannot continue for email " + receivedEmail.toString());
            }

            for (EmailForwardInSettings emailForwardInSettings : forwardInList) {
                // vytiahneme si usera o ktoreho sa jedna pre prijaty mail
                EmailReceived email = (EmailReceived) session.createCriteria(EmailReceived.class).add(Restrictions.eq("emailId", receivedEmail.getEmailId())).list().get(0);
                email.setEmailForwardInSettings(emailForwardInSettings);
                session.save(email);
                tx.commit();
                for (ClientSecureCommunicationData secureData : emailForwardInSettings.getRegisteredUser().getClientSecureCommunicationDatas()) {

                    MobileNumbers mobileNumber = secureData.getMobileNumbers();
                    LOG.debug(mobileNumber.getMobileNumber()  + " " + receivedEmail.getDestinationNumberList());
                    if (receivedEmail.getDestinationNumberList().contains("ALL")) {
                        // teraz vieme ze to chcele poslat vsade
                        if (isAbleToSendSecureMessage(secureData.getId())) {
                            smsHandler.sendSecureMessage(
                                    receivedEmail.getPayload().getBytes(),
                                    mobileNumber.getMobileNumber(),
                                    secureData.getId(),
                                    receivedEmail.getEmailId(), null);
                        }
                    } else if (receivedEmail.getDestinationNumberList().contains(mobileNumber.getMobileNumber())) {
                        if (isAbleToSendSecureMessage(secureData.getId())) {
                            smsHandler.sendSecureMessage(
                                    receivedEmail.getPayload().getBytes(),
                                    mobileNumber.getMobileNumber(),
                                    secureData.getId(),
                                    receivedEmail.getEmailId(), null);
                        }
                    }
                }

            }

            dbHandler.dbUpdateReceivedEmailStatus(receivedEmail.getEmailId(), EmailReceivedStatusTypeEnum.EMAIL_PROCESSED);
        } catch (UnableToRetrieveDataFromDb | HibernateException | KeyExchangeNotFinishedYetException | UnableToEncryptDataException ex) {

            LOG.error("Error processing new Email Message", ex);
            try {
                dbHandler.dbUpdateReceivedEmailStatus(receivedEmail.getEmailId(), EmailReceivedStatusTypeEnum.ERROR);
            } catch (UnableToRetrieveDataFromDb ex1) {
                LOG.error("Error updating receivedEmail Status Type " + ex1);
            }
            tx.rollback();
            throw new ErrorProcessingNewEmail("Error processing new email message", ex);
        } finally {
            session.close();
        }

    }

    public void processReceivedWsMessage(DeliverWsMessage receivedWs) throws WsNotFoundInInputForwardMappings, ErrorProcessingNewWs {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        LOG.debug("received ws " + receivedWs.toString());
        try {
            List<WsForwardInSettings> forwardInList = session.createCriteria(WsForwardInSettings.class)
                    .add(Restrictions.eq("wsAuthName", receivedWs.getName()))
                    .add(Restrictions.eq("wsAuthPassword", receivedWs.getPassword()))
                    .add(Restrictions.eq("sourceIpAddress", receivedWs.getSourceIpAddress()))
                    .list();

            if (forwardInList.isEmpty()) {
                throw new WsNotFoundInInputForwardMappings("Sender email address with identification id not found in DB, cannot continue for ws " + receivedWs.toString());
            }

            WsForwardInSettings wsForwardInSetting = forwardInList.get(0);

            // TOTO ZBEHNE IBA RAZ 
            for (ClientSecureCommunicationData secureData : wsForwardInSetting.getRegisteredUser().getClientSecureCommunicationDatas()) {
                MobileNumbers mobileNumber = secureData.getMobileNumbers();
                LOG.info("RECEIVED WS CHECKING MOBILE NUMBERS " + mobileNumber.getMobileNumber() + " received destination mobile number list " + receivedWs.getDestinationNumberList());
                if (receivedWs.getDestinationNumberList().contains("ALL")) {
                    // teraz vieme ze to chcele poslat vsade
                    if (isAbleToSendSecureMessage(secureData.getId())) {
                        smsHandler.sendSecureMessage(
                                receivedWs.getPayload().getBytes(),
                                mobileNumber.getMobileNumber(),
                                secureData.getId(),
                                null,
                                receivedWs.getWsUuid());
                    }
                } else if (receivedWs.getDestinationNumberList().contains(mobileNumber.getMobileNumber())) {
                    if (isAbleToSendSecureMessage(secureData.getId())) {
                        smsHandler.sendSecureMessage(
                                receivedWs.getPayload().getBytes(),
                                mobileNumber.getMobileNumber(),
                                secureData.getId(),
                                null,
                                receivedWs.getWsUuid());
                    }
                }

                UUID callbackUUID = UUID.randomUUID();

                smsHandler.registerDeliveryStatusSecureMessageCallback(callbackUUID, (DeliveryStatusSmsPDU receivedMessage, String emailUuid, String wsUuid) -> {
                    LOG.debug("BI DELIVERY CALLBACK received UUID for ws " + wsUuid);
                    if (receivedWs.getWsUuid().equals(wsUuid)) {
                        wsHandler.forwardMessageDeliveryResponseViaWs(secureData.getId(), wsUuid, receivedMessage);
                        smsHandler.unregisterDeliveryStatusSecureMessageCallback(callbackUUID);
                    }
                });
            }

            dbHandler.dbUpdateReceivedWsStatus(receivedWs.getWsUuid(), WsHandler.WsReceivedStatusEnum.WS_PROCESSING);
        } catch (UnableToRetrieveDataFromDb | HibernateException | KeyExchangeNotFinishedYetException | UnableToEncryptDataException ex) {

            LOG.error("Error processing new ws Message", ex);
            try {
                dbHandler.dbUpdateReceivedWsStatus(receivedWs.getWsUuid(), WsHandler.WsReceivedStatusEnum.WS_ERROR);
            } catch (UnableToRetrieveDataFromDb ex1) {
                LOG.error("Error updating receivedWs Status Type " + ex1);
            }
            tx.rollback();
            throw new ErrorProcessingNewWs("Error processing new ws message", ex);
        } finally {
            session.close();
        }

    }

    public void processReceivedSMSMessage(DeliverSmsPDU receivedSMSPdu) {
        String originatorNumber = receivedSMSPdu.getOriginatorNumber();
        LOG.debug("PROCESSING RECEIVED SMS MESSAGE");
        try {
            Set<BiMappings> biMappings = dbHandler.dbReadBiMappings(originatorNumber);
            LOG.debug("FOUND BI MAPPINGS " + biMappings.size());
            Boolean storeSms = true;
            SmsReceived smsReceivedDao = null;

            for (BiMappings biMapping : biMappings) {
                LOG.debug("STORING NEW SMS TO DB");
                if (storeSms) {
                    GatewayModems modem = dbHandler.dbReadGatewayModems(receivedSMSPdu.getModemInstance());

                    receivedSMSPdu.checkValidSecurePdu();

                    smsReceivedDao = new SmsReceived(
                            biMapping.getClientSecureCommunicationData(),
                            modem,
                            receivedSMSPdu.getPayloadHexString(),
                            receivedSMSPdu.getSmscNumber(),
                            receivedSMSPdu.getSmscNumberType(),
                            receivedSMSPdu.getSmscNumberingPlan(),
                            null,
                            receivedSMSPdu.getOriginatorNumber(),
                            receivedSMSPdu.getOriginatorNumberType(),
                            receivedSMSPdu.getOriginatorNumberingPlan(),
                            null,
                            receivedSMSPdu.getMessageUUID().toString(),
                            SmsReceivedStatusType.SMS_RECEIVED.toString(),
                            receivedSMSPdu.getSecurePdu().getSequenceNumber(), null, null, null);
                    dbHandler.dbWriteReceivedSms(smsReceivedDao);
                    storeSms = false;
                    LOG.debug(receivedSMSPdu.getSecurePdu());
                    if (receivedSMSPdu.getSecurePdu().getContentType() == SmsSecurePDU.ContentTypeEnum.KEY) {
                        processKeyExchangeProcedure(receivedSMSPdu);
                        break;
                    }
                }
                processBussinessLogic(receivedSMSPdu, biMapping);
            }

        } catch (UnableToRetrieveDataFromDb ex) {
            LOG.error("Unable to retrieve data from DB for received SMS PDU", ex);
            LOG.info("Saving received SMS as UNHANDLED");
            storeMessageAsUnhandled(receivedSMSPdu);
        } catch (UknownPduConentType ex) {
            LOG.error("Received message with unknown pdu contentType", ex);
            storeMessageAsUnhandled(receivedSMSPdu);
        }

    }

    public void storeMessageAsUnhandled(DeliverSmsPDU receivedSMSPdu) {
        try {
            GatewayModems modem = dbHandler.dbReadGatewayModems(receivedSMSPdu.getModemInstance());
            dbHandler.dbWriteUnhandledSms(new UnhandledSmsReceived(
                    null,
                    modem,
                    receivedSMSPdu.getPayloadHexString(),
                    receivedSMSPdu.getSmscNumber(),
                    receivedSMSPdu.getSmscNumberType(),
                    receivedSMSPdu.getSmscNumberingPlan(),
                    null,
                    receivedSMSPdu.getOriginatorNumber(),
                    receivedSMSPdu.getOriginatorNumberType(),
                    receivedSMSPdu.getOriginatorNumberingPlan(),
                    null,
                    receivedSMSPdu.getMessageUUID().toString(),
                    SmsReceivedStatusType.SMS_RECEIVED.toString()
            ));
        } catch (UnableToRetrieveDataFromDb ex1) {
            LOG.error("Unable to retrieve data from DB for received SMS PDU", ex1);
        }
    }

    private void processKeyExchangeProcedure(DeliverSmsPDU receivedSMSPdu) throws UnableToRetrieveDataFromDb, UknownPduConentType {
        LOG.info("RECEIVED KEY EXCHANGE MESSAGE");
        try {
            dbHandler.dbUpdateReceivedSmsStatus(receivedSMSPdu.getMessageUUID(), SmsReceivedStatusType.SMS_RECEIVED_PROCESSING);

            LOG.debug("SECURE PDU " + receivedSMSPdu.getSecurePdu().toString());
            processReceivedKeyMessage(receivedSMSPdu);
            dbHandler.dbUpdateReceivedSmsStatus(receivedSMSPdu.getMessageUUID(), SmsReceivedStatusType.SMS_RECEIVED_PROCESSED);
        } catch (UnableToRetrieveDataFromDb ex) {
            LOG.error("Unable to change received message status ", ex);
            dbHandler.dbUpdateReceivedSmsStatus(receivedSMSPdu.getMessageUUID(), SmsReceivedStatusType.SMS_RECEIVED_ERROR);
        }
    }

    private void processReceivedKeyExchangeRequest(DeliverSmsPDU keySmsPdu) throws UnableToRetrieveDataFromDb, UnableToGenerateSharedSecret, UknownPduConentType {
        LOG.info("[PHASE 1] Received key exchange request from " + keySmsPdu.toString());
        Session session = dbHandler.getNewSession();
        session.beginTransaction();
        SmsReceived receivedMessageDao = dbHandler.dbReadSmsReceived(keySmsPdu.getMessageUUID(), session);

        ClientSecureCommunicationData secureData = receivedMessageDao.getClientSecureCommunicationData();
        byte[] clientPublicKey = keySmsPdu.getSecurePdu().getPayload();
        byte[] sharedSecret = cypheringCurve.generateSharedSecret(keyHandler.getServerPrivateKeyByteArray(), clientPublicKey);
        secureData.setClientEccPublicKey(Utils.toHexString(clientPublicKey));
        secureData.setKeyNegotiationStatusType(KeyNegotiationStatusType.CLIENT_REQUEST_RECEIVED.toString());
        secureData.setSharedSecret(Utils.toHexString(sharedSecret));
        secureData.setAesKey(Utils.toHexString(cypheringCurve.generateAesKey(sharedSecret, Utils.toByteArray(secureData.getFirstApiKey()))));
        secureData.setApiKey(secureData.getFirstApiKey());
        secureData.setAesKeyModifiedAt(new Date());
        secureData.setKeyExchangeSeqNumber(keySmsPdu.getSecurePdu().getSequenceNumber());
        session.save(receivedMessageDao);
        session.getTransaction().commit();
        dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.CLIENT_PUB_KEY_STORED);
        // Teraz je potrebne odoslat verejny kluc serveru klientovi ktory poslal request
        LOG.info("[PHASE 2] New aes key generated for request" + keySmsPdu.toString());
        SubmitSmsPDU submitSms = new SubmitSmsPDU(keySmsPdu.getOriginatorNumber(), new SmsSecurePDUKey(
                secureData.getKeyExchangeSeqNumber(),
                SmsSecurePDU.ContentTypeEnum.KEY,
                SmsSecurePDU.CypherActiveEnum.OFF,
                SmsSecurePDU.KeyExchangeStageEnum.RESPONSE,
                keyHandler.getServerPublicKeyByteArray()));

        dbHandler.dbWriteSentSms(new SmsSent(
                secureData,
                null,
                null,
                receivedMessageDao,
                null,
                submitSms.getDestinationNumber(),
                Utils.toHexString(submitSms.getPayload()),
                submitSms.getMessageUUID().toString(),
                SmsSentStatusType.SMS_SENT.toString(),
                secureData.getKeyExchangeSeqNumber(),
                null));

        smsHandler.sendMessage(submitSms);
        LOG.info("[PHASE 3] Server ACK submitted for request" + keySmsPdu.toString());
        smsHandler.registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
            if (submitSms.getMessageUUID().equals(messageUuid)) {
                Session session1 = dbHandler.getNewSession();
                session1.beginTransaction();
                try {
                    SmsSent sentSms = dbHandler.dbReadSmsSent(messageUuid, session1);
                    GatewayModems gatewayModem = dbHandler.dbReadGatewayModems(modem);
                    sentSms.setGatewayModems(gatewayModem);
                    sentSms.setReferenceNumber(referenceNumber);
                    if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                        sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ACK_RECEIVED.toString());
                        dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.SERVER_PUB_KEY_SENT);
                        LOG.info("[PHASE 4] Server ACK successfully sent for request" + keySmsPdu.toString());
                    } else {
                        sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ERROR.toString());
                        dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.ERROR_SERVER_PUB_KEY_NOT_SENT);
                        LOG.error("[PHASE 4] Error sending server ACK for request" + keySmsPdu.toString());
                    }
                    session1.getTransaction().commit();
                } catch (UnableToRetrieveDataFromDb ex) {
                    LOG.error("Unable to change sent message status ", ex);
                } finally {
                    session1.close();
                }
            }
        });

    }

    private void processReceivedKeyExchangeAck(DeliverSmsPDU keySmsPdu) throws UnableToRetrieveDataFromDb, KeyExchangeSeqenceNumbersDoNotMatchException, UnableToDecryptDataException, UnableToGenerateMD5HashException, UnableToEncryptDataException, UknownPduConentType, KeyExchangeNotFinishedYetException {
        LOG.info("[PHASE 5] Client ACK received from " + keySmsPdu.toString());
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        SmsReceived receivedMessageDao = dbHandler.dbReadSmsReceived(keySmsPdu.getMessageUUID(), session);

        ClientSecureCommunicationData secureData = receivedMessageDao.getClientSecureCommunicationData();
        byte[] payload = keySmsPdu.getSecurePdu().getPayload();
        Boolean seqIdValid = secureData.getKeyExchangeSeqNumber().equals(keySmsPdu.getSecurePdu().getSequenceNumber());
        if (!seqIdValid) {
            LOG.error("[PHASE 6] Client ACK seq number not valid in message " + keySmsPdu.toString());
            secureData.setKeyNegotiationStatusType(KeyNegotiationStatusType.ERROR_CLIENT_SEQ_ID_NOT_VALID.toString());
        } else {
            LOG.info("[PHASE 6] Client ACK seq number is valid in message " + keySmsPdu.toString());
            secureData.setKeyNegotiationStatusType(KeyNegotiationStatusType.CLIENT_ACK_RECEIVED.toString());
        }
        session.save(receivedMessageDao);
        tx.commit();

        if (!seqIdValid) {
            throw new KeyExchangeSeqenceNumbersDoNotMatchException("Received ACK has invalid sequence number");
        }
        byte[] md5HashLocal;
        try {
            byte[] decryptedMd5Hash = decryptData(payload, secureData);

            md5HashLocal = cypheringCurve.getMD5Hash(Utils.toByteArray(secureData.getAesKey()));

            if (!Arrays.equals(md5HashLocal, decryptedMd5Hash)) {

                dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.ERROR_CLIENT_ACK_HASH_NOT_VALID);
                LOG.info("[PHASE 7] Unable to set keys generated, Client ACK has invalid MD5 HASH " + keySmsPdu.toString());
                throw new KeyExchangeSeqenceNumbersDoNotMatchException("Received ACK has invalid MD5 hash");
            }
        } catch (UnableToDecryptDataException | UnableToGenerateMD5HashException | KeyExchangeNotFinishedYetException ex) {
            LOG.error("[PHASE 7] Eror while decrypting md5 hash for " + keySmsPdu.toString());
            dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.ERROR);

            throw ex;
        }
        byte[] encryptedMD5Hash = null;
        try {

            encryptedMD5Hash = encryptData(md5HashLocal, secureData);
            LOG.info("[PHASE 8] Unable to set keys generated, Client ACK has invalid MD5 HASH " + keySmsPdu.toString());
        } catch (UnableToEncryptDataException ex) {
            LOG.error("[PHASE 8] Error while encrypting md5 hash when creating new ACK for requestAck " + keySmsPdu.toString());
            dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.ERROR_CLIENT_ACK_HASH_NOT_VALID);
            throw ex;
        }

        SubmitSmsPDU submitSms = new SubmitSmsPDU(keySmsPdu.getOriginatorNumber(), new SmsSecurePDUKey(
                secureData.getKeyExchangeSeqNumber(),
                SmsSecurePDU.ContentTypeEnum.KEY,
                SmsSecurePDU.CypherActiveEnum.OFF,
                SmsSecurePDU.KeyExchangeStageEnum.ACK,
                encryptedMD5Hash));

        dbHandler.dbWriteSentSms(new SmsSent(
                secureData,
                null,
                null,
                receivedMessageDao,
                null,
                submitSms.getDestinationNumber(),
                Utils.toHexString(submitSms.getPayload()),
                submitSms.getMessageUUID().toString(),
                SmsSentStatusType.SMS_SENT.toString(),
                secureData.getKeyExchangeSeqNumber(),
                null));
        LOG.info("[PHASE 9] Sending new ACK for requestACK " + keySmsPdu.toString());
        smsHandler.sendMessage(submitSms);
        smsHandler.registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
            if (submitSms.getMessageUUID().equals(messageUuid)) {
                Session session1 = dbHandler.getNewSession();

                Transaction tx1 = session1.beginTransaction();
                try {

                    SmsSent sentSms = dbHandler.dbReadSmsSent(messageUuid, session1);
                    GatewayModems gatewayModem = dbHandler.dbReadGatewayModems(modem);
                    sentSms.setGatewayModems(gatewayModem);
                    sentSms.setReferenceNumber(referenceNumber);
                    if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                        sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ACK_RECEIVED.toString());
                        dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.KEYS_GENERATED);
                        LOG.info("[PHASE 10] ACK successfully sent and new status KEYS_GENERATED were set to ClientData for requestACK " + keySmsPdu.toString());
                    } else {
                        sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ERROR.toString());
                        dbHandler.dbUpdateClientDataKeyExchangeStatus(secureData, KeyNegotiationStatusType.ERROR_SERVER_ACK_NOT_SENT);
                        LOG.error("[PHASE 10] ACK was not successfully sent and new status ERROR_SERVER_ACK_NOT_SENT were set to ClientData for requestACK " + keySmsPdu.toString());
                    }
                    session1.update(sentSms);
                    tx1.commit();
                } catch (UnableToRetrieveDataFromDb | HibernateException ex) {
                    LOG.error("[PHASE 10] unable to set new status to ClientData for requestACK " + keySmsPdu.toString(), ex);
                    tx1.rollback();
                } finally {
                    session.close();
                }
            }
        });

    }

    public void processReceivedDeliveryStatusMessage(DeliveryStatusSmsPDU deliveryStatusMessage) {
        LOG.debug("RECEIVED DELIVERY STATUS MESSAGE for ref num: " + deliveryStatusMessage.getReferenceNumber() + " from phone number : " + deliveryStatusMessage.getOriginatorNumber());
        Session session1 = dbHandler.getNewSession();
        Transaction tx = session1.beginTransaction();
        try {
            List<SmsSent> sentSmsList = session1.createCriteria(SmsSent.class, "smsSent")
                    .createAlias("smsSent.gatewayModems", "modem")
                    .add(Restrictions.eq("smsSent.referenceNumber", deliveryStatusMessage.getReferenceNumber()))
                    .add(Restrictions.eq("smsSent.destinationNumber", deliveryStatusMessage.getOriginatorNumber()))
                    .add(Restrictions.eq("modem.serialPort", deliveryStatusMessage.getModemInstance().getModemSerialPort()))
                    .add(Restrictions.eq("smsSent.smsSentStatusType", SmsSentStatusType.SMS_SENT_ACK_RECEIVED.toString()))
                    .addOrder(Order.desc("smsSent.timestamp"))
                    .list();
            SmsSent sentSms = null;
            if (sentSmsList.isEmpty()) {
                LOG.error("Received delivery status message for SMS not stored in DB from number: " + deliveryStatusMessage.getOriginatorNumber() + " ref number " + deliveryStatusMessage.getReferenceNumber());
                return;
            } else if (sentSmsList.size() > 1) {
                sentSms = sentSmsList.get(0);
                LOG.error("Received delivery status message for SMS from number: " + deliveryStatusMessage.getOriginatorNumber() + " ref number " + deliveryStatusMessage.getReferenceNumber() + " having multiple entries for ref number using last one");
            } else {
                sentSms = sentSmsList.get(0);
            }

            LOG.debug(sentSms.getMessageUuid());
            switch (deliveryStatusMessage.getDeliveryStatusType()) {
                case DELIVERED:
                    sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_DELIVERED.toString());
                    break;
                case REPEATING_DELIVERY:
                    sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_REPEATING_DELIVERY.toString());
                    break;
                case UNABLE_TO_DELIVER:
                    sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_UNABLE_TO_DELIVER.toString());
            }

            session1.update(sentSms);
            tx.commit();
        } catch (HibernateException ex) {
            tx.rollback();
        } finally {
            session1.close();
        }
        for (DeliveryStatusMessageReceivedCallback callback : deliveryStatusCallbackList) {
            callback.messageDeliveryStatusReceived(deliveryStatusMessage);
        }

    }

    synchronized public Integer getNextPduSeqNumber(Long clientId) throws UnableToRetrieveDataFromDb {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        Long oldNumber;
        try {
            ClientSecureCommunicationData clientData = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, clientId);
            oldNumber = clientData.getMessageSequenceNumber();
            clientData.setMessageSequenceNumber(oldNumber++ % 255);
            session.save(clientData);
            tx.commit();
        } catch (HibernateException ex) {
            tx.rollback();
            throw new UnableToRetrieveDataFromDb("Error while updating sequence number from DB for client " + clientId, ex);
        } finally {
            session.close();
        }
        return oldNumber.intValue();
    }

    public byte[] encryptData(byte[] data, Long clientId) throws KeyExchangeNotFinishedYetException, UnableToEncryptDataException, UnableToRetrieveDataFromDb {
        Session session = dbHandler.getNewSession();
        session.beginTransaction();
        ClientSecureCommunicationData clientDao = dbHandler.dbReadClientSecureCommunicationData(clientId, session);
        LOG.debug("ENCRYPTING WITH AES " + clientDao.getAesKey());
        LOG.debug("ENCRYPTING WITH API " + clientDao.getApiKey());
        byte[] encrypted = cypheringCurve.encryptData(data, Utils.toByteArray(clientDao.getAesKey()), Utils.toByteArray(clientDao.getApiKey()));
        LOG.debug("ENCRYPTED DATA " + Utils.toHexString(encrypted));
        session.close();
        return encrypted;
    }

    public byte[] encryptData(byte[] data, ClientSecureCommunicationData secureData) throws KeyExchangeNotFinishedYetException, UnableToEncryptDataException, UnableToRetrieveDataFromDb {

        LOG.debug("ENCRYPTING WITH AES " + secureData.getAesKey());
        LOG.debug("ENCRYPTING WITH API " + secureData.getApiKey());
        byte[] encrypted = cypheringCurve.encryptData(data, Utils.toByteArray(secureData.getAesKey()), Utils.toByteArray(secureData.getApiKey()));
        LOG.debug("ENCRYPTED DATA " + Utils.toHexString(encrypted));

        return encrypted;
    }

    public byte[] decryptData(byte[] data, Long clientId) throws KeyExchangeNotFinishedYetException, UnableToEncryptDataException, UnableToRetrieveDataFromDb, UnableToDecryptDataException {
        Session session = dbHandler.getNewSession();
        session.beginTransaction();
        ClientSecureCommunicationData clientDao = dbHandler.dbReadClientSecureCommunicationData(clientId, session);
        LOG.debug("DECRYPTING WITH AES " + clientDao.getAesKey());
        LOG.debug("DECRYPTING WITH API " + clientDao.getApiKey());
        byte[] decrypted = cypheringCurve.decryptData(data, Utils.toByteArray(clientDao.getAesKey()), Utils.toByteArray(clientDao.getApiKey()));
        LOG.debug("DECRYPTED DATA " + Utils.toHexString(decrypted));
        session.close();
        return decrypted;
    }

    public byte[] decryptData(byte[] data, ClientSecureCommunicationData secureData) throws KeyExchangeNotFinishedYetException, UnableToEncryptDataException, UnableToRetrieveDataFromDb, UnableToDecryptDataException {

        byte[] decrypted = cypheringCurve.decryptData(data, Utils.toByteArray(secureData.getAesKey()), Utils.toByteArray(secureData.getApiKey()));
        LOG.debug("DECRYPTING WITH AES " + secureData.getAesKey());
        LOG.debug("DECRYPTING WITH API " + secureData.getApiKey());
        LOG.debug("DECRYPTED DATA " + Utils.toHexString(decrypted));
        return decrypted;
    }

    public byte[] decryptData(byte[] data, ClientSecureCommunicationDataHistory secureData) throws KeyExchangeNotFinishedYetException, UnableToEncryptDataException, UnableToRetrieveDataFromDb, UnableToDecryptDataException {

        byte[] decrypted = cypheringCurve.decryptData(data, Utils.toByteArray(secureData.getAesKey()), Utils.toByteArray(secureData.getApiKey()));
        LOG.debug("DECRYPTING WITH AES " + secureData.getAesKey());
        LOG.debug("DECRYPTING WITH API " + secureData.getApiKey());
        LOG.debug("DECRYPTED DATA " + Utils.toHexString(decrypted));
        return decrypted;
    }

    private void processReceivedApiKeyRequest(DeliverSmsPDU keySmsPdu) throws UnableToRetrieveDataFromDb, UknownPduConentType, UnableToDecryptDataException, UnableToGenerateSharedSecret, UnableToGenerateMD5HashException, KeyExchangeNotFinishedYetException, KeyExchangeNotFinishedYetException, UnableToEncryptDataException {
        LOG.info("[PHASE 1] received API key request from " + keySmsPdu.toString());
        Session session = dbHandler.getNewSession();
        session.beginTransaction();
        SmsReceived receivedMessageDao = dbHandler.dbReadSmsReceived(keySmsPdu.getMessageUUID(), session);

        ClientSecureCommunicationData secureData = receivedMessageDao.getClientSecureCommunicationData();

        byte[] decrypted = decryptData(keySmsPdu.getSecurePdu().getPayload(), secureData.getId());
        SmsSecurePDUKey securePdu = (SmsSecurePDUKey) keySmsPdu.getSecurePdu();
        securePdu.setDecryptedPayload(decrypted);

        LOG.debug("Decrypted api key request " + securePdu.getTransmittedKey() + "for " + keySmsPdu.toString());
        byte[] newApiKey = securePdu.getTransmittedKeyByteArray();
        LOG.debug("Decrypted new api key " + Utils.toHexString(newApiKey));
        byte[] md5 = securePdu.getTransmittedMd5ByteArray();
        LOG.debug("RECEIVED MD5 " + Utils.toHexString(md5));
        byte[] newAesKey = cypheringCurve.generateAesKey(Utils.toByteArray(secureData.getSharedSecret()), newApiKey);

        if (!Arrays.equals(md5, cypheringCurve.getMD5Hash(newAesKey))) {
            LOG.error("[PHASE 2] received Api Key request invalid MD5 HASH from " + keySmsPdu.toString());
            dbHandler.dbUpdateClientDataKeyRenegotiationStatus(secureData, KeyNegotiationStatusType.ERROR);
            SubmitSmsPDU submitSms = new SubmitSmsPDU(keySmsPdu.getOriginatorNumber(), new SmsSecurePDUKey(
                    keySmsPdu.getSecurePdu().getSequenceNumber(),
                    SmsSecurePDU.ContentTypeEnum.KEY,
                    SmsSecurePDU.CypherActiveEnum.OFF,
                    SmsSecurePDU.KeyExchangeStageEnum.API_KEY_ACK_ERROR,
                    encryptData(cypheringCurve.getMD5Hash(Utils.toByteArray(secureData.getAesKey())), secureData)));

            dbHandler.dbWriteSentSms(new SmsSent(
                    secureData,
                    null,
                    null,
                    receivedMessageDao,
                    null,
                    submitSms.getDestinationNumber(),
                    Utils.toHexString(submitSms.getPayload()),
                    submitSms.getMessageUUID().toString(),
                    SmsSentStatusType.SMS_SENT.toString(),
                    secureData.getKeyExchangeSeqNumber(), null));

            smsHandler.sendMessage(submitSms);
            LOG.info("[PHASE 3] Api key ACK_ERROR sent " + keySmsPdu.toString());

        } else {
            LOG.info("[PHASE 2] received Api Key request MD5 hash VALID from " + keySmsPdu.toString());
            secureData.setKeyRenegotiationStatusType(KeyNegotiationStatusType.NEW_API_KEY_RECEIVED.toString());
            secureData.setNewApiKey(Utils.toHexString(newApiKey));
            secureData.setNewAesKeyModifiedAt(new Date());
            secureData.setNewApiKeyModifiedAt(new Date());

            secureData.setNewAesKey(Utils.toHexString(newAesKey));
            secureData.setKeyExchangeSeqNumber(keySmsPdu.getSecurePdu().getSequenceNumber());

            session.save(receivedMessageDao);
            session.getTransaction().commit();

            dbHandler.dbUpdateClientDataKeyRenegotiationStatus(secureData, KeyNegotiationStatusType.NEW_API_KEY_PREPARED);
            // Teraz je potrebne odoslat API KEY ACK klientovi
            SubmitSmsPDU submitSms = new SubmitSmsPDU(keySmsPdu.getOriginatorNumber(), new SmsSecurePDUKey(
                    secureData.getKeyExchangeSeqNumber(),
                    SmsSecurePDU.ContentTypeEnum.KEY,
                    SmsSecurePDU.CypherActiveEnum.ON,
                    SmsSecurePDU.KeyExchangeStageEnum.API_KEY_ACK_SUCCESS,
                    encryptData(cypheringCurve.getMD5Hash(newAesKey), secureData)));

            dbHandler.dbWriteSentSms(new SmsSent(
                    secureData,
                    null,
                    null,
                    receivedMessageDao,
                    null,
                    submitSms.getDestinationNumber(),
                    Utils.toHexString(submitSms.getPayload()),
                    submitSms.getMessageUUID().toString(),
                    SmsSentStatusType.SMS_SENT.toString(),
                    secureData.getKeyExchangeSeqNumber(), null));

            smsHandler.sendMessage(submitSms);
            LOG.info("[PHASE 3] Api key ACK_SUCCESS sent " + keySmsPdu.toString());
            smsHandler.registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
                if (submitSms.getMessageUUID().equals(messageUuid)) {

                    Session session1 = dbHandler.getNewSession();
                    session1.beginTransaction();
                    try {
                        SmsSent sentSms = dbHandler.dbReadSmsSent(messageUuid, session1);
                        GatewayModems gatewayModem = dbHandler.dbReadGatewayModems(modem);
                        sentSms.setGatewayModems(gatewayModem);
                        sentSms.setReferenceNumber(referenceNumber);
                        if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                            sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ACK_RECEIVED.toString());
                            dbHandler.dbUpdateClientDataKeyRenegotiationStatus(secureData, KeyNegotiationStatusType.NEW_API_KEY_ACK_SENT);
                            LOG.info("[PHASE 4] API_KEY_ACK_SUCCESS successfully sent " + keySmsPdu.toString());
                        } else {
                            sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ERROR.toString());
                            dbHandler.dbUpdateClientDataKeyRenegotiationStatus(secureData, KeyNegotiationStatusType.ERROR_NEW_API_KEY_ACK_NOT_SENT);
                            LOG.error("[PHASE 4] API_KEY_ACK_SUCCESS error sending " + keySmsPdu.toString());
                        }
                        session1.save(sentSms);
                        session1.getTransaction().commit();
                    } catch (UnableToRetrieveDataFromDb ex) {
                        LOG.error("Unable to change sent message status ", ex);
                    } finally {
                        session1.close();
                    }
                    // pockame si na to kym to pride k userovi uspesne a potom to tu mozme setnut 
                    registerDeliveryStatusCallback((DeliveryStatusSmsPDU receivedMessage) -> {
                        if (receivedMessage.getReferenceNumber().equals(referenceNumber) && receivedMessage.getOriginatorNumber().equals(submitSms.getDestinationNumber())) {
                            try {
                                dbHandler.dbUpdateClientDataKeyRenegotiationStatus(secureData, KeyNegotiationStatusType.NEW_API_KEY_GENERATED);
                                dbHandler.dbWriteClientSecureCommunicationDataHistoryRecord(secureData);
                                applyNewKeys(secureData);
                                LOG.info("[PHASE 5] NEW API KEYS were successfully applied for " + keySmsPdu.toString());
                            } catch (UnableToRetrieveDataFromDb ex) {
                                LOG.error("[PHASE 5] ERROR applying new API KEYS for " + keySmsPdu.toString(), ex);
                            }
                        }
                    });
                }
            });
        }
    }

    public boolean isAbleToSendSecureMessage(Long clientId) throws UnableToRetrieveDataFromDb {
        ClientSecureCommunicationData secureData = dbHandler.dbReadClientSecureCommunicationData(clientId);
        if (secureData.getKeyRenegotiationStatusType() != null && !secureData.getKeyRenegotiationStatusType().equals(KeyNegotiationStatusType.NEW_API_KEY_GENERATED.toString())) {
            switch (KeyNegotiationStatusType.valueOf(secureData.getKeyRenegotiationStatusType())) {
                case NEW_API_KEY_RECEIVED:
                case NEW_API_KEY_PREPARED:
                case NEW_API_KEY_ACK_SENT:
                    return false;
            }

        }

        if (secureData.getKeyNegotiationStatusType() == null || !secureData.getKeyNegotiationStatusType().equals(KeyNegotiationStatusType.KEYS_GENERATED.toString())) {
            return false;
        } else {
            return true;
        }
    }

    private void applyNewKeys(ClientSecureCommunicationData secureData) throws UnableToRetrieveDataFromDb {
        LOG.debug("Applying new KEYS after negotiation");
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            ClientSecureCommunicationData secureDataNew = dbHandler.dbReadClientSecureCommunicationData(secureData.getId(), session);
            secureDataNew.setAesKey(secureDataNew.getNewAesKey());
            secureDataNew.setApiKey(secureDataNew.getNewApiKey());
            secureDataNew.setAesKeyModifiedAt(new Date());
            secureDataNew.setApiKeyModifiedAt(new Date());
            session.save(secureDataNew);
            tx.commit();
        } catch (HibernateException ex) {
            LOG.error("Error while applying new keys", ex);
            tx.rollback();
        } finally {
            session.close();
        }
    }

    public void registerDeliveryStatusCallback(DeliveryStatusMessageReceivedCallback callback) {
        deliveryStatusCallbackList.add(callback);
    }

    public void registerMessageSentStatusCallback(WebServiceProcessedCallback callback) {
        wsMessageProcessedCallbackList.add(callback);
    }

    public ServerGuiOperations getServerGuiOpsHandler() {
        return serverGuiOpsHandler;
    }

    public WsHandler getWsHandler() {
        return wsHandler;
    }

}
