/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.compression;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.CompressionHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.jsonObjects.CustomAlphabet;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;

/**
 *
 * @author martinhudec
 */
public class CompressionHandlerServer extends CompressionHandler {

    private static CompressionHandlerServer instance;
    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(CompressionHandlerServer.class);

    public CompressionHandlerServer() {
    }

    public static CompressionHandlerServer getInstance() {
        if (instance == null) {
            instance = new CompressionHandlerServer();
        }
        return instance;
    }


    public CustomAlphabet loadCustomAlphabetFromJson(String customAlphabet) throws FileNotFoundException {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        CustomAlphabet data = gson.fromJson(customAlphabet, CustomAlphabet.class);
        return data;
    }
}
