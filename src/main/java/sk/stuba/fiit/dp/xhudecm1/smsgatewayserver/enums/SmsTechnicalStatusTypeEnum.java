/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums;

/**
 *
 * @author martinhudec
 */
public enum SmsTechnicalStatusTypeEnum {
    SERVER_CLIENT_TECHNICAL_SENT, SERVER_CLIENT_TECHNICAL_ACK_RECEIVED, CLIENT_SERVER_TECHNICAL_RECEIVED, CLIENT_SERVER_TECHNICAL_ACK_SENT
}
