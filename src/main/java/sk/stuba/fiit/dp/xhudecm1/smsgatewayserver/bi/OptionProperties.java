/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi;

import com.google.gson.annotations.Expose;
import java.util.List;

/**
 *
 * @author martinhudec
 */
public class OptionProperties {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(OptionProperties.class);
    @Expose
    private List<Modems> modems;
    @Expose
    private String hibernateConfigPath;
    @Expose
    private String gmailClientSecretPath;
    @Expose
    private String serverEmailAddress;

    public OptionProperties() {
    }

    public List<Modems> getModems() {
        return modems;
    }

    public void setModems(List<Modems> modems) {
        this.modems = modems;
    }

    public String getHibernateConfigPath() {
        return hibernateConfigPath;
    }

    public String getGmailClientSecretPath() {
        return gmailClientSecretPath;
    }

    public void setGmailClientSecretPath(String gmailClientSecretPath) {
        this.gmailClientSecretPath = gmailClientSecretPath;
    }

    public String getServerEmailAddress() {
        return serverEmailAddress;
    }

    public void setServerEmailAddress(String serverEmailAddress) {
        this.serverEmailAddress = serverEmailAddress;
    }

    @Override
    public String toString() {
        return "OptionProperties [modems=" + modems
                + ", gmailClientSecretPath=" + gmailClientSecretPath
                + ", hibernateConfigPath=" + hibernateConfigPath + "]";
    }

    public class Modems {

        @Expose
        private String name;
        @Expose
        private String manufacturer;
        @Expose
        private String simPin;
        @Expose
        private String serialPort;
        @Expose
        private String operator;
        @Expose
        private String IMEI;
        @Expose
        private String mobileNumber;

        public Modems() {
        }

        public Modems(String name, String manufacturer, String simPin, String serialPort, String operator, String IMEI, String mobileNumber) {
            this.name = name;
            this.manufacturer = manufacturer;
            this.simPin = simPin;
            this.serialPort = serialPort;
            this.operator = operator;
            this.IMEI = IMEI;
            this.mobileNumber = mobileNumber;
        }

        public String getSimPin() {
            return simPin;
        }

        public void setSimPin(String simPin) {
            this.simPin = simPin;
        }

        public String getName() {

            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public String getSerialPort() {
            return serialPort;
        }

        public void setSerialPort(String serialPort) {
            this.serialPort = serialPort;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String Operator) {
            this.operator = Operator;
        }

        public void setIMEI(String IMEI) {
            this.IMEI = IMEI;
        }

        public String getIMEI() {
            return IMEI;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        @Override
        public String toString() {
            return "(name=" + name
                    + ", manufacturer=" + manufacturer
                    + ", serialPort=" + serialPort
                    + ", operator=" + operator
                    + ", IMEI=" + IMEI
                    + ", mobileNumber=" + mobileNumber
                    + ", simPin=" + simPin + ")";
        }
    }
}
