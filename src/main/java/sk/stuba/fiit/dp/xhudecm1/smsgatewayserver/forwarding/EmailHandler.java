/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.model.History;
import com.google.api.services.gmail.model.HistoryMessageAdded;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartHeader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums.EmailReceivedStatusTypeEnum;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.EmailNotFoundInInputForwardMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.ErrorProcessingNewEmail;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoDataFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoDataTypeFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoDestinationNumbersFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoIdentificationkeyFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToRetrieveDataFromDb;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.BiMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailForwardOutSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;

/**
 *
 * @author martinhudec
 */
public class EmailHandler implements EmailHandlerInterface {

    private final DbHandler dbHandler = DbHandler.getInstance();

    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(EmailHandler.class);
    private final BusinessLogicHandler biHandler;

    public EmailHandler(BusinessLogicHandler biHandler) {
        initEmailHandler();
        this.biHandler = biHandler;
    }

    private void initEmailHandler() {
        if (dbHandler.dbReadEmailMessagesCount() == 0) {
            try {
                List<Message> messageList = GmailInstance.listAllMessages();
                processNewMessageList(messageList);
                new Thread(new EmailThread()).start();
            } catch (IOException ex) {
                Logger.getLogger(EmailHandler.class.getName()).log(Level.SEVERE, null, ex);

            }
        } else {
            new Thread(new EmailThread()).start();
        }
    }

    @Override
    public void sendEmail(DeliverSmsPDU receivedSmsPdu, Set<EmailForwardOutSettings> emailForwardSettings) throws UknownPduConentType {
        try {
            for (EmailForwardOutSettings emailForwardSetting : emailForwardSettings) {
                if (emailForwardSetting.getActiveStatus().equals("ACTIVE")) {
                    String body = replaceValues(receivedSmsPdu, emailForwardSetting.getBody());
                    String subject = replaceValues(receivedSmsPdu, emailForwardSetting.getSubject());
                    GmailInstance.sendEmail(emailForwardSetting.getDestinationEmailAddress(),
                            body,
                            subject);

                    try {
                        SmsReceived receivedSmsDao = dbHandler.dbReadSmsReceived(receivedSmsPdu.getMessageUUID());

                        dbHandler.dbWriteSentEmail(new EmailSent(emailForwardSetting, receivedSmsDao,
                                body,
                                subject,
                                emailForwardSetting.getDestinationEmailAddress(),
                                new Date(), SentEmailMessageType.EMAIL_DATA.toString()));
                    } catch (UnableToRetrieveDataFromDb ex) {
                        LOG.error("Error while storing sent email in DB", ex);
                    }
                }
            }
        } catch (IOException | MessagingException ex) {
            LOG.error("Error while sending email", ex);
        }
    }

    private String replaceValues(DeliverSmsPDU receivedSmsPdu, String toBeReplaced) throws UknownPduConentType {
        String replacedString = toBeReplaced.replaceAll("%phoneNumber%", receivedSmsPdu.getOriginatorNumber());
        replacedString = replacedString.replaceAll("%body%", new String(receivedSmsPdu.getSecurePdu().getPureData()));
        return replacedString;
    }

    private class EmailThread implements Runnable {

        @Override
        public void run() {
            while (true) {

                try {
     
                    List<History> historyList = GmailInstance.listHistory(dbHandler.dbReadLastEmailHistoryId());
                    for (History history : historyList) {
                        List<Message> newMessageList = new ArrayList<>();
                        if (history.getMessagesAdded() != null) {
                            for (HistoryMessageAdded addedMessage : history.getMessagesAdded()) {
                                newMessageList.add(addedMessage.getMessage());
                            }
                            processNewMessageList(newMessageList);
                        }
                    }

                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(EmailHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(EmailHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    private String getEmailData(List<MessagePart> messageParts) {

        for (MessagePart part : messageParts) {
            LOG.debug(part.getMimeType());
            if ("text/plain".equals(part.getMimeType()) || "text/html".equals(part.getMimeType())) {
                return new String(Base64.decodeBase64(part.getBody().getData()));
            } else {
                return getEmailData(part.getParts());
            }

        }
        return null;
    }

    private void processNewMessageList(List<Message> messageList) {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {

            for (Message message : messageList) {
                Message messageData = GmailInstance.getMessage(message.getId());
                if (messageData.getLabelIds().contains("UNREAD") && messageData.getLabelIds().contains("INBOX")) {
                    try {
                        String from = null;
                        String subject = null;
                        String emailData;
                        String fromEmailAddress = null;
                        for (MessagePartHeader header : messageData.getPayload().getHeaders()) {
                            switch (header.getName()) {
                                case "From":
                                    from = header.getValue();
                                    break;
                                case "Subject":
                                    subject = header.getValue();
                                    break;
                            }
                        }
                        fromEmailAddress = Utils.getRegexValue(from, "[\\w!#$%&’*+=?`{|}~^-]+(?:\\.[\\w!#$%&’*+=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}").trim();

                        if ("text/plain".equals(messageData.getPayload().getMimeType()) || "text/html".equals(messageData.getPayload().getMimeType())) {
                            emailData = new String(Base64.decodeBase64(messageData.getPayload().getBody().getData()));
                        } else {
                            emailData = getEmailData(messageData.getPayload().getParts());
                        }
                        EmailReceived newEmail = new EmailReceived();
                        newEmail.setSourceMailAddress(fromEmailAddress);
                        newEmail.setSubject(subject);
                        newEmail.setData(emailData);
                        newEmail.setCreatedAt(new Date());
                        newEmail.setEmailId(messageData.getId());
                        newEmail.setEmailInternalDate(messageData.getInternalDate());
                        newEmail.setEmailReceivedStatusType(EmailReceivedStatusTypeEnum.EMAIL_RECEIVED.toString());
                        newEmail.setEmailHistoryId(messageData.getHistoryId() == null ? null : messageData.getHistoryId().longValue());
                        newEmail.setEmailSnippet(messageData.getSnippet());
                        newEmail.setEmailThreadId(messageData.getThreadId());

                        session.save(newEmail);
                        tx.commit();
                        dbHandler.dbUpdateReceivedEmailStatus(messageData.getId(), EmailReceivedStatusTypeEnum.EMAIL_PROCESSING);
                        // teraz potrebujeme prevolat callback ktory odpali bussineslogic handler

                        biHandler.processReceivedEmailMessage(new DeliverEmailMessage(
                                fromEmailAddress,
                                subject,
                                emailData,
                                messageData.getId(),
                                messageData));

                    } catch (Utils.UnableToFindValueWithRegexException | ErrorProcessingNewEmail | UnableToRetrieveDataFromDb | NoDestinationNumbersFoundInEmailException | NoDataFoundInEmailException | NoDataTypeFoundInEmailException ex) {
                        LOG.error("Error processing new email message", ex);
                        try {
                            dbHandler.dbUpdateReceivedEmailStatus(messageData.getId(), EmailReceivedStatusTypeEnum.ERROR);

                        } catch (UnableToRetrieveDataFromDb ex1) {
                            LOG.error("Unable to update received email status", ex1);
                        }

                        // TODO send response email with error unavailable to process
                    } catch (EmailNotFoundInInputForwardMappings ex) {
                        LOG.error("Email not found in input mappings ", ex);
                        // TODO send response email ze nemozem pokracovat lebo tu nemam co robit
                    } catch (NoIdentificationkeyFoundInEmailException ex) {
                        LOG.error("No identification found in mail", ex);
                        try {
                            dbHandler.dbUpdateReceivedEmailStatus(messageData.getId(), EmailReceivedStatusTypeEnum.ERROR_NO_IDENTIFICATION_KEY);

                        } catch (UnableToRetrieveDataFromDb ex1) {
                            LOG.error("Unable to update received email status", ex1);
                        }
                    }
                }
            }

        } catch (IOException ex) {
            LOG.error("Error while processing new mail list", ex);
            tx.rollback();
        } finally {
            session.close();
        }
    }

    public enum SentEmailMessageType {
        EMAIL_DATA, EMAIL_DELIVERY_RESPONSE
    }
}
