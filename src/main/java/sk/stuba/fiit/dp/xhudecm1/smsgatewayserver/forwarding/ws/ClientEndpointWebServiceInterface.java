/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

/**
 *
 * @author martinhudec
 */
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService()
@SOAPBinding(style = Style.RPC)
public interface ClientEndpointWebServiceInterface {

    @WebMethod(operationName = "sentMessageDelivered")
    String sentMessageDelivered(@WebParam(name = "messageUuid") String uuid,
            @WebParam(name = "deliveryStatus") String deliveryStatus);

    @WebMethod(operationName = "secureSmsMessageReceived")
    String secureSmsMessageReceived(@WebParam(name = "messageUuid") String uuid,
            @WebParam(name = "originatorNumber") String originatorNumber,
            @WebParam(name = "data") String data);

}
