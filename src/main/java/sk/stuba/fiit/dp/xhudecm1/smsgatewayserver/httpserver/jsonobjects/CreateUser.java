/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects;

import com.google.gson.annotations.Expose;

/**
 *
 * @author martinhudec
 */
public class CreateUser {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CreateUser.class);
    @Expose
    private String userEmail;

    public CreateUser(String userEmail) {
        this.userEmail = userEmail;
    }

    public CreateUser() {
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        return "CreateUser [userEmail=" + userEmail + "]";
    }

}
