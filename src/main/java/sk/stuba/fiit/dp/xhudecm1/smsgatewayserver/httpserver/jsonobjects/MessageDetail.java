/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects;

import com.google.gson.annotations.Expose;

/**
 *
 * @author martinhudec
 */
public class MessageDetail {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(MessageDetail.class);
    @Expose
    private String payload;
    @Expose
    private String compressionType;
    @Expose
    private String contentType;
    @Expose
    private String destinationType;
    

    public MessageDetail() {
    }

    public MessageDetail(String payload, String compressionType, String contentType, String destinationType) {
        this.payload = payload;
        this.compressionType = compressionType;
        this.contentType = contentType;
        this.destinationType = destinationType;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(String compressionType) {
        this.compressionType = compressionType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getDestinationType() {
        return destinationType;
    }

    public void setDestinationType(String destinationType) {
        this.destinationType = destinationType;
    }

   
    @Override
    public String toString() {
        return "CreateUser [destinationType=" + destinationType
                + ", contentType=" + contentType
                + ", payload=" + payload
                + ", compressionType=" + compressionType + "]";
    }

}
