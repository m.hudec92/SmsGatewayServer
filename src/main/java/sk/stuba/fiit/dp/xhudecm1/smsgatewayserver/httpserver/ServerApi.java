package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.LoggerFactory;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.jsonObjects.CustomAlphabet;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.App;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.ServerGuiOperations;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UserDbOpertaionException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.CreateUser;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.GenerateCustomAlphabet;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.MessageDetail;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.RegisterNewDevice;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects.SendTestMessage;

/**
 *
 * @author martinhudec
 */
public class ServerApi extends HttpServlet {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(ServerApi.class);
    ServerGuiOperations serverGuiOps;

    public ServerApi(ServerGuiOperations serverGuiOps) {
        this.serverGuiOps = serverGuiOps;
    }

    // private Session session;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json; charset=UTF-8");
        logger.info("[req URI]: " + request.getRequestURI());

        String line = null;

        BufferedReader reader = request.getReader();
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        String postDataString = sb.toString();
        logger.debug("Post data string " + postDataString);
        switch (request.getRequestURI()) {
            case "/api/createUser": {
                logger.debug("[CREATE USER]");
                CreateUser createUserData = (CreateUser) loadPostDataJson(postDataString, CreateUser.class);
                try {
                    if (createUserData.getUserEmail() != null) {
                        serverGuiOps.createNewUser(createUserData.getUserEmail());
                        response.setStatus(HttpServletResponse.SC_OK);
                        return;
                    }
                } catch (UserDbOpertaionException ex) {
                    logger.error("Exception during creating new user ", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                logger.debug(createUserData.toString());
                break;
            }
            case "/api/registerNewDevice": {
                logger.debug("registering new device");
                RegisterNewDevice regNewDev = (RegisterNewDevice) loadPostDataJson(postDataString, RegisterNewDevice.class);
                logger.debug("new request for registering new device " + regNewDev.toString());
                try {
                    if (regNewDev.getUserEmail() != null && regNewDev.getMobileNumber() != null) {
                        serverGuiOps.registerNewDeviceForUser(regNewDev);
                        response.setStatus(HttpServletResponse.SC_OK);
                        return;
                    }
                } catch (UserDbOpertaionException ex) {
                    logger.error("Exception during creating new user ", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                break;
            }
            case "/api/generateCustomAlphabet": {
                logger.debug("generating new alphabet");
                GenerateCustomAlphabet genCustomAlph = (GenerateCustomAlphabet) loadPostDataJson(postDataString, GenerateCustomAlphabet.class);
                logger.debug("new request for generating custom alphabet " + genCustomAlph.toString());
                try {
                    if (genCustomAlph.getUserEmail() != null && genCustomAlph.getDataSet() != null) {
                        serverGuiOps.generateCustomAlphabet(genCustomAlph);
                        response.setStatus(HttpServletResponse.SC_OK);
                        return;
                    } else {
                        response.getOutputStream().write(("invalid call " + request.getRequestURI()).getBytes());
                        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    }
                } catch (UserDbOpertaionException ex) {
                    logger.error("Exception during creating new user ", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                break;
            }
            case "/api/sendTechnicalPingMessage": {
                logger.debug("sending test message");
                SendTestMessage sendTestMessage = (SendTestMessage) loadPostDataJson(postDataString, SendTestMessage.class);
                logger.debug("new request for sending test message " + sendTestMessage.toString());
                try {
                    if (sendTestMessage.getUserEmail() != null && sendTestMessage.getDeviceId() != null) {

                        serverGuiOps.sendTestMessage(sendTestMessage);
                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        response.getOutputStream().write(("invalid call " + request.getRequestURI()).getBytes());
                        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    }
                } catch (UserDbOpertaionException ex) {
                    logger.error("Exception during creating new user ", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                break;
            }
            case "/api/sendKeyExchangeRequest": {
                logger.debug("sending sendKeyExchangeRequest message");
                SendTestMessage sendTestMessage = (SendTestMessage) loadPostDataJson(postDataString, SendTestMessage.class);
                logger.debug("new request for sending sendKeyExchangeRequest message " + sendTestMessage.toString());
                try {
                    if (sendTestMessage.getUserEmail() != null && sendTestMessage.getDeviceId() != null) {

                        serverGuiOps.sendKeyExchangeRequest(sendTestMessage);
                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        response.getOutputStream().write(("invalid call " + request.getRequestURI()).getBytes());
                        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    }
                } catch (UserDbOpertaionException ex) {
                    logger.error("Exception during creating new user ", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                break;
            }
            default: {
                response.getOutputStream().write(("invalid call " + request.getRequestURI()).getBytes());
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/json");
        logger.debug("[req URI]: " + request.getRequestURI());

        Map<String, String[]> parameterMap = request.getParameterMap();
        logger.debug(parameterMap.toString());

        switch (request.getRequestURI()) {
            case "/api/getServerApiWsdl": {
                logger.debug("api getting wsdl");
                response.getOutputStream().write((serverGuiOps.getServerWsdl().getBytes()));
                break;
            }
            case "/api/getServerApiEmail": {
                logger.debug("api getting email");
                response.getOutputStream().write((App.OPTION_PROPERTIES.getServerEmailAddress().getBytes()));
                break;
            }
            case "/api/getMessageDetail": {
                try {
                    MessageDetail detail = serverGuiOps.getMessageDetail(request.getParameter("messageId"));
                    response.getOutputStream().write(loadDataToJson(detail, MessageDetail.class).getBytes());
                } catch (UserDbOpertaionException ex) {
                    logger.error("exception while loading message detail ", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                break;
            }
            case "/api/getSentMessageDetail": {
                try {
                    MessageDetail detail = serverGuiOps.getSentMessageDetail(request.getParameter("messageId"));

                    response.getOutputStream().write(loadDataToJson(detail, MessageDetail.class).getBytes());
                } catch (UserDbOpertaionException ex) {
                    logger.error("exception while loading message detail ", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                break;
            }

            default: {
                response.getOutputStream().write(("invalid call " + request.getRequestURI()).getBytes());
            }
        }
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private Object loadPostDataJson(String jsonData, Class classType) throws FileNotFoundException {
        logger.debug("loading post data to json post data -> " + jsonData);
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        Object data = gson.fromJson(jsonData, classType);
        return data;
    }

    private String loadDataToJson(Object data, Class classType) throws FileNotFoundException {

        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        String json = gson.toJson(data, classType);
        return json;
    }
}
