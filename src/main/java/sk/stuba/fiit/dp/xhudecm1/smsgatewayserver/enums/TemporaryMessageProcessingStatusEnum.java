/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums;

/**
 *
 * @author martinhudec
 */
public enum TemporaryMessageProcessingStatusEnum {
    PROCESSED,NOT_PROCESSED,ERROR
}
