/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

/**
 *
 * @author martinhudec
 */
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface WebServiceInterface {

    @WebMethod(operationName = "sendSecureMessage")
    WebServiceSendResult sendSecureSmsMessage(@WebParam(name = "destination")String destination, @WebParam(name = "data")String data);
    
}
