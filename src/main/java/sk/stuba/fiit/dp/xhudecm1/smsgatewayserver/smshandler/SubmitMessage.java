/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.smshandler;

import java.util.UUID;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;

/**
 *
 * @author martinhudec
 */
public class SubmitMessage {

    private final byte[] data;

    private final SmsSecurePDU securePdu;
    private final UUID messageUUID;

    public SubmitMessage(byte[] data, SmsSecurePDU securePdu, UUID messageUUID) {
        this.data = data;
        this.securePdu = securePdu;
        this.messageUUID = messageUUID;
    }

    public byte[] getData() {
        return data;
    }

    public SmsSecurePDU getSecurePdu() {
        return securePdu;
    }

    public UUID getMessageUUID() {
        return messageUUID;
    }

}
