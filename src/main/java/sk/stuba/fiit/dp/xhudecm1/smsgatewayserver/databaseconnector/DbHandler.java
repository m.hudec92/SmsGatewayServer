/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector;

import java.io.File;
import java.security.KeyPair;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.App;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.OptionProperties.Modems;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.cyphering.CypheringCurve;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums.EmailReceivedStatusTypeEnum;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums.OperatorTypeEnum;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.OptionPropertiesException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToGenerateKeyPairs;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToRetrieveDataFromDb;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UserDbOpertaionException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws.WsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.BiMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClassOperatorType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationDataHistory;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.GatewayModems;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.MobileNumbers;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.RegisteredUser;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.TemporarySmsStorage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.UnhandledSmsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.KeyNegotiationStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.SmsReceivedStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.SmsSentStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;

/**
 *
 * @author martinhudec
 */
public class DbHandler {

    private SessionFactory sessionFactory = null;
    private CypheringCurve cypheringCurve = null;
    private Session readSession = null;
    private static DbHandler instance;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DbHandler.class);

    private DbHandler() {
        File config = new File(App.OPTION_PROPERTIES.getHibernateConfigPath());
        Configuration configuration = new Configuration().configure(config);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.build());
        cypheringCurve = CypheringCurve.getInstance();
        readSession = sessionFactory.openSession();
        checkModemsFromConfiguration();
    }

    public static DbHandler getInstance() {
        if (instance == null) {
            instance = new DbHandler();
        }
        return instance;
    }

    public void dbWriteReceivedSms(SmsReceived smsReceived) {
        dbWriteObject(smsReceived);
    }

    public void dbWriteSentSms(SmsSent smsSent) {
        dbWriteObject(smsSent);
    }

    public void dbWriteUnhandledSms(UnhandledSmsReceived unhandledSmsReceived) {
        dbWriteObject(unhandledSmsReceived);
    }

    private void dbWriteObject(Object o) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(o);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("Error while storing data to DB  ", e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateReceivedSmsStatus(UUID receivedMessageUUID, SmsReceivedStatusType status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating received sms status for uuid " + receivedMessageUUID.toString());
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<SmsReceived> receivedMessagesList = session.createCriteria(SmsReceived.class).add(Restrictions.eq("messageUuid", receivedMessageUUID.toString())).list();
            SmsReceived receivedMessage = receivedMessagesList.get(0);

            receivedMessage.setSmsReceivedStatusType(status.toString());
            session.update(receivedMessage);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("Error while storing updating SMS received status ", e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateSentSmsStatus(UUID receivedMessageUUID, SmsSentStatusType status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating received sms status for uuid " + receivedMessageUUID.toString());
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<SmsSent> sentMessagesList = session.createCriteria(SmsSent.class).add(Restrictions.eq("messageUuid", receivedMessageUUID.toString())).list();
            SmsSent sentMessage = sentMessagesList.get(0);

            sentMessage.setSmsSentStatusType(status.toString());
            session.update(sentMessagesList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("Error while storing updating SMS received status ", e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateSentWsStatus(String sentWsUuid, WsHandler.WsSentStatusEnum status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating sent ws status for uuid " + sentWsUuid.toString());
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<WsSent> sentMessagesList = session.createCriteria(WsSent.class).add(Restrictions.eq("wsMessageUuid", sentWsUuid.toString())).list();
            WsSent wsSent = sentMessagesList.get(0);

            wsSent.setWsSentStatusType(status.toString());
            session.update(wsSent);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("Error while storing updating WS sent status ", e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateSentWsReceivedStatus(String receivedWsUuid, WsHandler.WsReceivedStatusEnum status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating received ws status for uuid " + receivedWsUuid.toString());
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<WsReceived> receivedWsList = session.createCriteria(WsReceived.class).add(Restrictions.eq("messageUuid", receivedWsUuid.toString())).list();
            WsReceived wsReceived = receivedWsList.get(0);

            wsReceived.setWsReceivedStatusType(status.toString());
            session.update(receivedWsList);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("Error while storing updating WS received status ", e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateClientDataKeyExchangeStatus(ClientSecureCommunicationData client, KeyNegotiationStatusType status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating client keyExchangestatus " + status.toString() + " for client " + client.getId());
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            ClientSecureCommunicationData clientDao = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, client.getId());

            clientDao.setKeyNegotiationStatusType(status.toString());
            session.update(clientDao);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("updating client keyExchangestatus " + status.toString() + " for client " + client.getId(), e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateClientDataKeyRenegotiationStatus(ClientSecureCommunicationData client, KeyNegotiationStatusType status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating client keyExchangestatus " + status.toString() + " for client " + client.getId());
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            ClientSecureCommunicationData clientDao = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, client.getId());

            clientDao.setKeyRenegotiationStatusType(status.toString());
            session.update(clientDao);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("updating client keyExchangestatus " + status.toString() + " for client " + client.getId(), e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateReceivedEmailStatus(String emailId, EmailReceivedStatusTypeEnum status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating received email status " + status.toString() + " for email id  " + emailId);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            List<EmailReceived> emailDao = session.createCriteria(EmailReceived.class).add(Restrictions.eq("emailId", emailId)).list();
            if (emailDao.isEmpty()) {
                throw new UnableToRetrieveDataFromDb("No records in DB for email ID " + emailId);
            }

            emailDao.get(0).setEmailReceivedStatusType(status.toString());
            session.update(emailDao.get(0));
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("error updating received email status " + status.toString() + " for email " + emailId, e);
        } finally {
            session.close();
        }
    }

    public void dbUpdateReceivedWsStatus(String wsMessageUuid, WsHandler.WsReceivedStatusEnum status) throws UnableToRetrieveDataFromDb {
        Session session = sessionFactory.openSession();
        logger.debug("updating received ws status " + status.toString() + " for ws id  " + wsMessageUuid);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            List<WsReceived> wsDao = session.createCriteria(WsReceived.class).add(Restrictions.eq("wsMessageUuid", wsMessageUuid)).list();
            if (wsDao.isEmpty()) {
                throw new UnableToRetrieveDataFromDb("No records in DB for ws ID " + wsMessageUuid);
            }

            wsDao.get(0).setWsReceivedStatusType(status.toString());
            session.update(wsDao.get(0));
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("error updating received ws status " + status.toString() + " for ws " + wsMessageUuid, e);
        } finally {
            session.close();
        }
    }

    public void dbWriteSentEmail(EmailSent email) {
        logger.debug("Storing new email to DB" + email.toString());
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(email);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("ERROR while storing new sent email to DB", e);
        } finally {
            session.close();
        }
    }

    public void dbWriteSentWs(WsSent ws) {
        logger.debug("Storing new ws to DB" + ws.toString());
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(ws);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("ERROR while storing new sent ws to DB", e);
        } finally {
            session.close();
        }
        logger.debug("STORRED");
    }

    public Set<BiMappings> dbReadBiMappings(String number) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB bi mappings for number " + number);

        try {
            List<MobileNumbers> mobileNumberList = readSession.createCriteria(MobileNumbers.class).add(Restrictions.eq("mobileNumber", number)).list();
            for (MobileNumbers mobileNumber : mobileNumberList) {
                logger.debug("listing mobile numbers" + mobileNumber.getMobileNumber());
                for (ClientSecureCommunicationData clientSecureCommunicationData : mobileNumber.getClientSecureCommunicationDatas()) {
                    if (!clientSecureCommunicationData.getBiMappingses().isEmpty()) {
                        return clientSecureCommunicationData.getBiMappingses();
                    }
                }
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for number " + number, e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for mobileNumber " + number);

    }

    public ClientSecureCommunicationData dbReadClientSecureCommunicationData(Long clientId) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB client key exchange status " + clientId);

        try {
            ClientSecureCommunicationData clientDao = (ClientSecureCommunicationData) readSession.get(ClientSecureCommunicationData.class, clientId);
            logger.debug("client aes key " + clientDao.getAesKey());
            return clientDao;
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to get data for client " + clientId, e);
        }
    }

    public ClientSecureCommunicationData dbReadClientSecureCommunicationData(Long clientId, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB client key exchange status " + clientId);

        try {
            ClientSecureCommunicationData clientDao = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, clientId);
            return clientDao;
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to get data for client " + clientId, e);
        }
    }

    public MobileNumbers dbReadMobileNumber(String mobileNumber, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB mobile number data " + mobileNumber);

        try {
            List<MobileNumbers> mobileNumbersList = session.createCriteria(MobileNumbers.class).add(Restrictions.eq("mobileNumber", mobileNumber)).list();
            if (mobileNumbersList.isEmpty()) {
                throw new UnableToRetrieveDataFromDb("Unable to get data for mobileNumber " + mobileNumber);
            }
            return mobileNumbersList.get(0);
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to get data for mobileNumber " + mobileNumber, e);
        }
    }

    public SmsReceived dbReadSmsReceived(UUID messageUuid) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB sms received message " + messageUuid.toString());

        try {
            List<SmsReceived> receivedMessagesList = readSession.createCriteria(SmsReceived.class).add(Restrictions.eq("messageUuid", messageUuid.toString())).list();
            for (SmsReceived message : receivedMessagesList) {
                logger.debug("listing messages" + messageUuid.toString());
                return message;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for message uuid " + messageUuid.toString(), e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for messageUUID " + messageUuid.toString());

    }

    public SmsReceived dbReadSmsReceived(UUID messageUuid, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB sms received message " + messageUuid.toString());

        try {
            List<SmsReceived> receivedMessagesList = session.createCriteria(SmsReceived.class).add(Restrictions.eq("messageUuid", messageUuid.toString())).list();
            for (SmsReceived message : receivedMessagesList) {
                logger.debug("listing messages" + messageUuid.toString());
                return message;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for message uuid " + messageUuid.toString(), e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for messageUUID " + messageUuid.toString());

    }

    public RegisteredUser dbReadRegisteredUser(String email, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB registered user by email  " + email);

        try {
            List<RegisteredUser> registeredUserList = session.createCriteria(RegisteredUser.class).add(Restrictions.eq("email", email)).list();
            for (RegisteredUser user : registeredUserList) {
                logger.debug("listing users" + email);
                return user;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for user with email -> " + email, e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for user with email ->  " + email);
    }

    public EmailReceived dbReadEmailReceived(String emailId, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB email received message " + emailId);

        try {
            List<EmailReceived> emailDao = session.createCriteria(EmailReceived.class).add(Restrictions.eq("emailId", emailId)).list();
            if (emailDao.isEmpty()) {
                throw new UnableToRetrieveDataFromDb("No records in DB for email ID " + emailId);
            }
            return emailDao.get(0);

        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for email ID  " + emailId, e);
        }

    }

    public WsReceived dbReadWsReceived(String wsId, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB ws received message " + wsId);

        try {
            List<WsReceived> wsDao = session.createCriteria(WsReceived.class).add(Restrictions.eq("wsMessageUuid", wsId)).list();
            if (wsDao.isEmpty()) {
                throw new UnableToRetrieveDataFromDb("No records in DB for ws ID " + wsId);
            }
            return wsDao.get(0);

        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for ws ID  " + wsId, e);
        }

    }

    public SmsSent dbReadSmsSent(UUID messageUuid) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB sms sent message " + messageUuid.toString());

        try {
            List<SmsSent> sentMessageList = readSession.createCriteria(SmsSent.class).add(Restrictions.eq("messageUuid", messageUuid.toString())).list();
            for (SmsSent message : sentMessageList) {
                logger.debug("listing messages" + messageUuid.toString());
                return message;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for sent message uuid " + messageUuid.toString(), e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for sent messageUUID " + messageUuid.toString());

    }

    public SmsSent dbReadSmsSent(UUID messageUuid, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB sms sent message " + messageUuid.toString());

        try {
            List<SmsSent> sentMessageList = session.createCriteria(SmsSent.class).add(Restrictions.eq("messageUuid", messageUuid.toString())).list();
            for (SmsSent message : sentMessageList) {
                logger.debug("listing messages" + messageUuid.toString());
                return message;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for sent message uuid " + messageUuid.toString(), e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for sent messageUUID " + messageUuid.toString());

    }

    public RegisteredUser dbReadUser(String email) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB userData with email " + email);

        try {
            List<RegisteredUser> userList = readSession.createCriteria(RegisteredUser.class).add(Restrictions.eq("email", email)).list();

            for (RegisteredUser user : userList) {
                logger.debug("listing  users " + user.getUserName());
                return user;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for user " + email, e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for user " + email);

    }

    public List<GatewayModems> dbReadAllGatewayModems(Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB all modems");
        try {
            List<GatewayModems> modemList = session.createCriteria(GatewayModems.class).add(Restrictions.ne("status", "INACTIVE")).list();

            return modemList;
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read all modems from DB", e);
        }

    }

    public GatewayModems dbReadGatewayModems(ModemInstance modem) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB modem info for modem " + modem.getModemSerialPort());

        try {
            List<GatewayModems> modemList = readSession.createCriteria(GatewayModems.class).add(Restrictions.eq("serialPort", modem.getModemSerialPort())).add(Restrictions.ne("status", "INACTIVE")).list();

            for (GatewayModems gatewayModem : modemList) {
                logger.debug("listing  modems" + gatewayModem.getName());
                return gatewayModem;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for modem " + modem.getModemSerialPort(), e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for modem " + modem.getModemSerialPort());

    }

    public GatewayModems dbReadGatewayModems(ModemInstance modem, Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB modem info for modem " + modem.getModemSerialPort());

        try {
            List<GatewayModems> modemList = session.createCriteria(GatewayModems.class).add(Restrictions.eq("serialPort", modem.getModemSerialPort())).list();

            for (GatewayModems gatewayModem : modemList) {
                logger.debug("listing  modems" + gatewayModem.getName());
                return gatewayModem;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for modem " + modem.getModemSerialPort(), e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for modem " + modem.getModemSerialPort());

    }

    public ClassOperatorType dbReadOperator(OperatorTypeEnum operator) throws UnableToRetrieveDataFromDb {
        logger.debug("READING from DB operator type " + operator);

        try {
            List<ClassOperatorType> operatorList = readSession.createCriteria(ClassOperatorType.class).add(Restrictions.eq("value", operator)).list();

            for (ClassOperatorType currentOperator : operatorList) {
                logger.debug("listing  operators" + currentOperator.getName());
                return currentOperator;
            }
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to read data for operator " + operator, e);
        }
        throw new UnableToRetrieveDataFromDb("No data found for operator " + operator);

    }

    public List<TemporarySmsStorage> dbReadAllTemporaryStoredMessages(Session session) throws UnableToRetrieveDataFromDb {
        logger.debug("Listing all temporaryily stored messages");
        try {
            List<TemporarySmsStorage> tempMessagesList = session.createCriteria(TemporarySmsStorage.class).list();
            return tempMessagesList;
        } catch (HibernateException e) {
            throw new UnableToRetrieveDataFromDb("Unable to list temporary stored messages", e);
        }

    }

    public void dbWriteClientSecureCommunicationDataHistoryRecord(ClientSecureCommunicationData secureData) {
        logger.info("Creating new history record for client secure communication data ");
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            ClientSecureCommunicationDataHistory userDataHistory;

            userDataHistory = new ClientSecureCommunicationDataHistory();
            userDataHistory.setAesKey(secureData.getAesKey());
            userDataHistory.setAesKeyModifiedAt(secureData.getAesKeyModifiedAt());
            userDataHistory.setApiKey(secureData.getApiKey());
            userDataHistory.setApiKeyModifiedAt(secureData.getApiKeyModifiedAt());
            userDataHistory.setClientEccPublicKey(secureData.getClientEccPublicKey());
            userDataHistory.setClientSecureCommunicationData(secureData);
            userDataHistory.setKeyExchangeSeqNumber(secureData.getKeyExchangeSeqNumber());
            userDataHistory.setKeyNegotiationStatusType(secureData.getKeyNegotiationStatusType());
            userDataHistory.setKeyRenegotiationStatusType(secureData.getKeyRenegotiationStatusType());
            userDataHistory.setMessageSequenceNumber(secureData.getMessageSequenceNumber());
            userDataHistory.setMobileNumbers(secureData.getMobileNumbers());
            userDataHistory.setNewAesKey(secureData.getNewAesKey());
            userDataHistory.setNewAesKeyModifiedAt(secureData.getNewAesKeyModifiedAt());
            userDataHistory.setNewApiKey(secureData.getNewApiKey());
            userDataHistory.setNewApiKeyModifiedAt(secureData.getNewApiKeyModifiedAt());
            userDataHistory.setRegisteredUser(secureData.getRegisteredUser());
            userDataHistory.setSharedSecret(secureData.getSharedSecret());
            userDataHistory.setUsedForm(secureData.getAesKeyModifiedAt());
            userDataHistory.setUsedTo(new Date());
            session.save(userDataHistory);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("ERROR while creating new client secure communication history data ", e);
        } finally {
            session.close();
        }
    }

    public Session getReadSession() {
        return readSession;
    }

    public Session getNewSession() {
        return sessionFactory.openSession();
    }

    public Integer dbReadEmailMessagesCount() {
        return readSession.createCriteria(EmailReceived.class).setProjection(Projections.rowCount()).uniqueResult().hashCode();
    }

    public Long dbReadLastEmailHistoryId() {
        return ((EmailReceived) readSession.createCriteria(EmailReceived.class)
                .addOrder(Order.desc("id")).
                setMaxResults(1).uniqueResult())
                .getEmailHistoryId();
    }

    public void dbWriteRegisteredUser(String name, String email) throws UserDbOpertaionException {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {

            RegisteredUser newUser = new RegisteredUser();
            newUser.setCreatedAt(new Date());
            newUser.setModifiedAt(new Date());
            newUser.setUserName(name);
            newUser.setEmail(email);
            newUser.setUserUuid(((UUID) generateUUID()).toString());
            session.save(newUser);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("ERROR while storing new user to DB", e);
            throw new UserDbOpertaionException("Exception during creating client secure comm data", e);
        } finally {
            session.close();
        }
    }

    private UUID generateUUID() {
        return UUID.randomUUID();
    }

    public void dbWriteClientSecureCommunicationData(RegisteredUser registeredUser, MobileNumbers mobileNumber, Session session, String deviceName, String description) throws UnableToGenerateKeyPairs {
        ClientSecureCommunicationData userData;
        userData = new ClientSecureCommunicationData();
        KeyPair apiKey = cypheringCurve.generateKeyPairs();
        String compressedApiKey = Utils.toHexString(cypheringCurve.compressPublicKey(apiKey.getPublic()));
        userData.setApiKey(compressedApiKey);
        userData.setFirstApiKey(compressedApiKey);
        userData.setApiKeyModifiedAt(new Date());
        userData.setRegisteredUser(registeredUser);
        userData.setMobileNumbers(mobileNumber);
        userData.setDeviceName(deviceName);
        userData.setMessageSequenceNumber(0l);
        userData.setDescription(description);
        userData.setModifiedAt(new Date());
        userData.setCreatedAt(new Date());
        logger.debug("writing new client secure communication data ");
        session.save(userData);
        ClassOperatorType operator = (ClassOperatorType) session.createCriteria(ClassOperatorType.class).add(Restrictions.eq("value", "ORANGE")).list().get(0);
        // potrebne vytvorit bi mappings 
        BiMappings biMappings = new BiMappings(); 
        biMappings.setCreatedAt(new Date());
        biMappings.setModifiedAt(new Date());
        biMappings.setClientSecureCommunicationData(userData);
        biMappings.setClassOperatorType(operator);
        biMappings.setBiOperationType("WS");
        session.save(biMappings);
        BiMappings biMappingsEmail = new BiMappings(); 
        biMappingsEmail.setCreatedAt(new Date());
        biMappingsEmail.setModifiedAt(new Date());
        biMappingsEmail.setClientSecureCommunicationData(userData);
        biMappingsEmail.setClassOperatorType(operator);
        biMappingsEmail.setBiOperationType("EMAIL");
        session.save(biMappingsEmail);
    }

    private void checkModemsFromConfiguration() {

        logger.info("checking configured modems angainst DB " + App.OPTION_PROPERTIES.getModems().toString());
        List<Modems> modems = App.OPTION_PROPERTIES.getModems();

        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<GatewayModems> allModemsList = session.createCriteria(GatewayModems.class).add(Restrictions.ne("status", "INACTIVE")).list();
            for (GatewayModems gatewayModems : allModemsList) {
                Boolean match = false;
                for (Modems modem : modems) {
                    if (modem.getSerialPort().equals(gatewayModems.getSerialPort())) {
                        match = true;
                    }
                }
                if (!match) {
                    gatewayModems.setStatus("INACTIVE");
                    session.update(gatewayModems);
                }
            }
            for (Modems modem : modems) {
                Boolean addNewModem = false;
                List<GatewayModems> modemsDbList = session.createCriteria(GatewayModems.class)
                        .add(Restrictions.eq("serialPort", modem.getSerialPort()))
                        .add(Restrictions.or(Restrictions.ne("status", "INACTIVE"), Restrictions.and(Restrictions.eq("status", "INACTIVE"), Restrictions.eq("imei", modem.getIMEI()))))
                        .list();
                if (!modemsDbList.isEmpty()) {
                    GatewayModems currentModem = modemsDbList.get(0);
                    if (modem.getIMEI().equals(currentModem.getImei())) {
                        currentModem.setManufacturer(modem.getManufacturer());
                        currentModem.setName(modem.getName());
                        currentModem.setPhoneNumber(modem.getMobileNumber());
                        List<ClassOperatorType> operator = session.createCriteria(ClassOperatorType.class).add(Restrictions.eq("value", modem.getOperator())).list();
                        if (operator.isEmpty()) {
                            throw new RuntimeException(new OptionPropertiesException("Invalid operator set for modem"));
                        }
                        currentModem.setClassOperatorType(operator.get(0));
                        currentModem.setStatus("ACTIVE");
                        session.saveOrUpdate(currentModem);
                    } else {
                        currentModem.setStatus("INACTIVE");
                        session.saveOrUpdate(currentModem);
                        addNewModem = true;
                    }
                } else {
                    addNewModem = true;
                }
                if (addNewModem) {
                    GatewayModems currentModem = new GatewayModems();
                    List<ClassOperatorType> operator = session.createCriteria(ClassOperatorType.class).add(Restrictions.eq("value", modem.getOperator())).list();
                    if (operator.isEmpty()) {
                        throw new RuntimeException(new OptionPropertiesException("Invalid operator set for modem"));
                    }
                    currentModem.setClassOperatorType(operator.get(0));
                    currentModem.setManufacturer(modem.getManufacturer());
                    currentModem.setName(modem.getName());
                    currentModem.setPhoneNumber(modem.getMobileNumber());
                    currentModem.setImei(modem.getIMEI());
                    currentModem.setSerialPort(modem.getSerialPort());
                    currentModem.setSimPin(modem.getSimPin());
                    currentModem.setStatus("ACTIVE");
                    session.saveOrUpdate(currentModem);
                }

            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error("ERROR while storing new modems to DB", e);
            throw new RuntimeException(new OptionPropertiesException("Invalid operator set for modem", e));
        } finally {
            session.close();
        }
    }
}
