/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.cyphering;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Date;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToGetServerKeysFromStorage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class ServerKeyHandler {

    private static ServerKeyHandler instance;
    private ServerKeyObject keyObject;
    private String KEY_STORAGE = System.getProperty("user.home") + "/.sms_gateway/server_keys.json";
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CypheringCurve.class);

    public ServerKeyHandler() throws UnableToGetServerKeysFromStorage {
        try {
            keyObject = loadServerKeys();
            if (keyObject.getPrivateKey() == null || keyObject.getPublicKey() == null){
                throw new UnableToGetServerKeysFromStorage("Error getting server keys from storage");
            }
        } catch (FileNotFoundException ex) {
            throw new UnableToGetServerKeysFromStorage("Error getting server keys from storage", ex);
        }
    }

    private ServerKeyObject loadServerKeys() throws FileNotFoundException {
        logger.info("LOADING SERVER KEYS");
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        BufferedReader reader = new BufferedReader(new FileReader(KEY_STORAGE));
        ServerKeyObject data = gson.fromJson(reader, ServerKeyObject.class);
        logger.debug(data);
        logger.info("SERVER KEYS SUCCESFULLY LOADED");
        return data;
    }

    public static ServerKeyHandler getInstance() throws UnableToGetServerKeysFromStorage {
        if (instance == null) {
            instance = new ServerKeyHandler();
        }
        return instance;

    }

    public byte[] getServerPrivateKeyByteArray() {
        return keyObject.getPrivateKeyByteArray();
    }

    public byte[] getServerPublicKeyByteArray() {
        return keyObject.getPublicKeyByteArray();
    }

    public String getServerPrivateKey() {
        return keyObject.getPrivateKey();
    }

    public String getServerPublicKey() {
        return keyObject.getPublicKey();
    }

    private class ServerKeyObject {

        @Expose
        private String eccPrivateKey;
        @Expose
        private String eccPublicKey;
        @Expose
        private Date createdAt;
        @Expose
        private Date modifiedAt;

        public ServerKeyObject(String privateKey, String publicKey, Date createdAt, Date modifiedAt) {
            this.eccPrivateKey = privateKey;
            this.eccPublicKey = publicKey;
            this.createdAt = createdAt;
            this.modifiedAt = modifiedAt;
        }

        public ServerKeyObject() {
        }

        public byte[] getPrivateKeyByteArray() {
            return Utils.toByteArray(eccPrivateKey);
        }

        public byte[] getPublicKeyByteArray() {
            return Utils.toByteArray(eccPublicKey);
        }

        public String getPrivateKey() {
            return eccPrivateKey;
        }

        public void setPrivateKey(String privateKey) {
            this.eccPrivateKey = privateKey;
        }

        public String getPublicKey() {
            return eccPublicKey;
        }

        public void setPublicKey(String publicKey) {
            this.eccPublicKey = publicKey;
        }

        public Date getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
        }

        public Date getModifiedAt() {
            return modifiedAt;
        }

        public void setModifiedAt(Date modifiedAt) {
            this.modifiedAt = modifiedAt;
        }

        @Override
        public String toString() {
            return "ServerKeys [eccPublicKey=" + eccPublicKey + ", eccPrivateKey=" + eccPrivateKey + ", "
                    + "modifiedAt=" + modifiedAt + ", createdAt=" + createdAt + "]";
        }
    }
}
