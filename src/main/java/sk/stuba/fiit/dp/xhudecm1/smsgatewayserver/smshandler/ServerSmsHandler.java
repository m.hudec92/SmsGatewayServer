/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.smshandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.CompressedData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.compression.CompressionHandlerServer;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.enums.TemporaryMessageProcessingStatusEnum;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.KeyExchangeNotFinishedYetException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToRetrieveDataFromDb;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.loadbalancer.LoadBalancer;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.GatewayModems;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.TemporarySmsStorage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsReceived;

import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliveryStatusSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.MessageSentResultCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUTechnical;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.ModemStatus;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.SmsSentStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UnableToLoadModemConfiguration;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialGsmModemCommunicatorException;

/**
 *
 * @author martinhudec
 */
public class ServerSmsHandler extends SmsHandler {

    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ServerSmsHandler.class);
    private final LoadBalancer loadBalancer;
    private final BusinessLogicHandler biHandler;
    private DbHandler dbHandler;
    private final Map<UUID, DeliveryStatusSecureMessageCallback> secureMessageDeliveryStatusCallbackList = new ConcurrentHashMap<>();
    private final CompressionHandlerServer compressionHandler = CompressionHandlerServer.getInstance();

    public ServerSmsHandler() throws Exception {
        super();
        loadBalancer = new LoadBalancer(modemList);
        biHandler = new BusinessLogicHandler(this);
    }

    @Override
    protected void specifyModems() throws UnableToLoadModemConfiguration {
        dbHandler = DbHandler.getInstance();
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            LOG.debug("SPECIFING MODEMS");
            List<GatewayModems> modems = dbHandler.dbReadAllGatewayModems(session);
            for (GatewayModems modem : modems) {
                LOG.debug("initializing modem " + modem.getSerialPort());
                try {
                    ModemInstance newModemInstance = null;                 
                    newModemInstance = new ModemInstance(modem.getSerialPort(), "SM", null, modem.getClassOperatorType().getValue());
                    modemList.add(newModemInstance);
                    modem.setStatus(ModemStatus.CONNECTED.toString());
                    LOG.debug("SETTING MODEM STATUS CONNECTED " + modem.getSerialPort());
                } catch (SerialGsmModemCommunicatorException ex) {
                    modem.setStatus(ModemStatus.ERROR_SERIAL_NOT_CONNECTED.toString());
                    LOG.error("Unable to initialize modem " + modem.getSerialPort(), ex);
                }
                session.save(modem);

            }
            tx.commit();
        } catch (UnableToRetrieveDataFromDb | HibernateException ex) {
            throw new UnableToLoadModemConfiguration("Error while loading gateway modems from DB", ex);
        } finally {
            session.close();
        }
    }

    @Override
    protected ModemInstance getModem(SubmitSmsPDU pdu) {
        return loadBalancer.getAppropriateModem(pdu);
    }

    @Override
    protected void processReceivedMessage(DeliverSmsPDU receivedMessage) {
        LOG.info("PROCESSING RECEIVED MESSAGE " + receivedMessage.getOriginatorNumber());
        biHandler.processReceivedSMSMessage(receivedMessage);
    }

    @Override
    protected void processReceivedDeliveryStatusMessage(DeliveryStatusSmsPDU deliveryStatusMessage) {
        biHandler.processReceivedDeliveryStatusMessage(deliveryStatusMessage);
    }

    public void registerDeliveryStatusSecureMessageCallback(UUID callbackUUID, DeliveryStatusSecureMessageCallback callback) {
        secureMessageDeliveryStatusCallbackList.put(callbackUUID, callback);
    }

    public void unregisterDeliveryStatusSecureMessageCallback(UUID callbackUUID) {
        secureMessageDeliveryStatusCallbackList.remove(callbackUUID);
    }

    public void sendTechnicalSecureMessage(SmsSecurePDU.TechnicalMessageTypeEnum technicalMessageType, byte[] data, String destinationNumber, Long clientId) throws UnableToRetrieveDataFromDb, KeyExchangeNotFinishedYetException, UnableToEncryptDataException {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            ClientSecureCommunicationData secureData = dbHandler.dbReadClientSecureCommunicationData(clientId, session);

            if (biHandler.isAbleToSendSecureMessage(clientId)) {
                CompressedData compressedData = compressionHandler.compressData(data, null);
                LOG.debug("Compressed Data " + new String(compressedData.getCompressed()));

                SmsSecurePDU securePDU = new SmsSecurePDUTechnical(
                        biHandler.getNextPduSeqNumber(clientId),
                        SmsSecurePDU.ContentTypeEnum.TECHNICAL,
                        SmsSecurePDU.CypherActiveEnum.ON,
                        compressedData.getCompressionType(),
                        technicalMessageType,
                        biHandler.encryptData(
                                compressedData.getCompressed(),
                                secureData.getId()));

                SubmitSmsPDU submitPDU = new SubmitSmsPDU(
                        destinationNumber,
                        securePDU);
                sendMessage(submitPDU);
                SubmitMessage submitMessage = new SubmitMessage(data, securePDU, submitPDU.getMessageUUID());

                SmsSent smsSent = new SmsSent(
                        secureData,
                        null,
                        null,
                        null,
                        null,
                        destinationNumber,
                        Utils.toHexString(submitPDU.getPayload()),
                        submitMessage.getMessageUUID().toString(),
                        SmsSentStatusType.SMS_SENT.toString(),
                        submitMessage.getSecurePdu().getSequenceNumber(),
                        null);
                session.save(smsSent);
                tx.commit();
                registerMessageSentResultCallback((UUID messageUuid, Integer messageReferenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
                    if (submitMessage.getMessageUUID().equals(messageUuid)) {
                        Session session1 = dbHandler.getNewSession();
                        session1.beginTransaction();
                        try {
                            SmsSent sentSms = dbHandler.dbReadSmsSent(messageUuid, session1);
                            GatewayModems gatewayModem = dbHandler.dbReadGatewayModems(modem, session1);
                            sentSms.setGatewayModems(gatewayModem);
                            sentSms.setReferenceNumber(messageReferenceNumber);
                            if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                                sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ACK_RECEIVED.toString());
                            } else {
                                sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ERROR.toString());
                            }
                            session1.save(sentSms);
                            session1.getTransaction().commit();
                        } catch (UnableToRetrieveDataFromDb ex) {
                            LOG.error("Unable to change sent message status ", ex);
                        } finally {
                            session1.close();
                        }
                    }
                });
            } else {
                TemporarySmsStorage tempSms = new TemporarySmsStorage(
                        secureData,
                        null,
                        null,
                        Utils.toHexString(data),
                        destinationNumber,
                        TemporaryMessageProcessingStatusEnum.NOT_PROCESSED.toString());
                session.save(tempSms);
                tx.commit();
            }
        } catch (HibernateException ex) {
            LOG.error("Unable to store secure SMS status to DB  ", ex);
            tx.rollback();
        } catch (IOException ex) {
            Logger.getLogger(ServerSmsHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            session.close();
        }
    }

    public void sendSecureMessage(byte[] data, String destinationNumber, Long clientId, String emailUuid, String wsUuid) throws UnableToRetrieveDataFromDb, KeyExchangeNotFinishedYetException, UnableToEncryptDataException {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            ClientSecureCommunicationData secureData = dbHandler.dbReadClientSecureCommunicationData(clientId, session);
            EmailReceived receivedEmail = null;
            WsReceived receivedWs = null;
            if (emailUuid != null) {
                receivedEmail = dbHandler.dbReadEmailReceived(emailUuid, session);
            } else if (wsUuid != null) {
                receivedWs = dbHandler.dbReadWsReceived(wsUuid, session);
            }
            if (biHandler.isAbleToSendSecureMessage(clientId)) {
                CompressedData compressedData = compressionHandler.compressData(data, compressionHandler.loadCustomAlphabetFromJson(secureData.getCustomAlphabet()));
                LOG.debug("Compressed Data " + new String(compressedData.getCompressed()));

                SmsSecurePDU securePDU = new SmsSecurePDUData(
                        biHandler.getNextPduSeqNumber(clientId),
                        SmsSecurePDU.ContentTypeEnum.DATA,
                        SmsSecurePDU.CypherActiveEnum.ON,
                        compressedData.getCompressionType(),
                        SmsSecurePDU.DestinationTypeEnum.SMS,
                        biHandler.encryptData(
                                compressedData.getCompressed(),
                                secureData.getId()));

                SubmitSmsPDU submitPDU = new SubmitSmsPDU(
                        destinationNumber,
                        securePDU);
                sendMessage(submitPDU);
                SubmitMessage submitMessage = new SubmitMessage(data, securePDU, submitPDU.getMessageUUID());

                SmsSent smsSent = new SmsSent(
                        secureData,
                        receivedEmail,
                        null,
                        null,
                        receivedWs,
                        destinationNumber,
                        Utils.toHexString(submitPDU.getPayload()),
                        submitMessage.getMessageUUID().toString(),
                        SmsSentStatusType.SMS_SENT.toString(),
                        submitMessage.getSecurePdu().getSequenceNumber(),
                        null);
                session.save(smsSent);
                tx.commit();
                registerMessageSentResultCallback((UUID messageUuid, Integer messageReferenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
                    if (submitMessage.getMessageUUID().equals(messageUuid)) {
                        Session session1 = dbHandler.getNewSession();
                        session1.beginTransaction();
                        try {
                            SmsSent sentSms = dbHandler.dbReadSmsSent(messageUuid, session1);
                            GatewayModems gatewayModem = dbHandler.dbReadGatewayModems(modem, session1);
                            sentSms.setGatewayModems(gatewayModem);
                            sentSms.setReferenceNumber(messageReferenceNumber);
                            if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                                sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ACK_RECEIVED.toString());
                            } else {
                                sentSms.setSmsSentStatusType(SmsSentStatusType.SMS_SENT_ERROR.toString());
                            }
                            session1.save(sentSms);
                            session1.getTransaction().commit();
                            biHandler.registerDeliveryStatusCallback((DeliveryStatusSmsPDU receivedMessage) -> {
                                LOG.debug("RECEIVED DELIVERY CALLBACK for sms uuid: " + sentSms.getMessageUuid() + " ws uuid " + wsUuid);
                                if (receivedMessage.getOriginatorNumber().equals(smsSent.getDestinationNumber()) && receivedMessage.getReferenceNumber().equals(sentSms.getReferenceNumber())) {
                                    for (Map.Entry<UUID, DeliveryStatusSecureMessageCallback> entry : secureMessageDeliveryStatusCallbackList.entrySet()) {
                                        UUID key = entry.getKey();
                                        DeliveryStatusSecureMessageCallback callback = entry.getValue();
                                        callback.messageDeliveryStatusReceived(receivedMessage, emailUuid, wsUuid);
                                    }
                                }
                            });
                        } catch (UnableToRetrieveDataFromDb ex) {
                            LOG.error("Unable to change sent message status ", ex);
                        } finally {
                            session1.close();
                        }
                    }
                });

            } else {
                TemporarySmsStorage tempSms = new TemporarySmsStorage(
                        secureData,
                        receivedEmail,
                        receivedWs,
                        Utils.toHexString(data),
                        destinationNumber,
                        TemporaryMessageProcessingStatusEnum.NOT_PROCESSED.toString());
                session.save(tempSms);
                tx.commit();
            }
        } catch (HibernateException ex) {
            LOG.error("Unable to store secure SMS status to DB  ", ex);
            tx.rollback();
        } catch (IOException ex) {
            Logger.getLogger(ServerSmsHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            session.close();
        }
    }

    public void sendTemporarilyStoredMessages() {
        LOG.debug("Security LOGIC available, sending stored messages");
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        try {
            List<TemporarySmsStorage> tempMessageList = dbHandler.dbReadAllTemporaryStoredMessages(session);
            for (TemporarySmsStorage message : tempMessageList) {
                try {
                    sendSecureMessage(Utils.toByteArray(message.getData()), message.getDestinationNumber(), message.getClientSecureCommunicationData().getId(), message.getEmailReceived().getEmailId(), message.getWsReceived().getWsMessageUuid());
                } catch (UnableToRetrieveDataFromDb | KeyExchangeNotFinishedYetException | UnableToEncryptDataException ex) {
                    message.setProcessingStatus(TemporaryMessageProcessingStatusEnum.ERROR.toString());
                }
                message.setProcessingStatus(TemporaryMessageProcessingStatusEnum.PROCESSED.toString());
                session.save(message);
            }

            tx.commit();
        } catch (UnableToRetrieveDataFromDb | HibernateException ex) {
            LOG.error("Error while sending temporarily stored messages", ex);
            tx.rollback();
        } finally {
            session.close();
        }
    }

    public BusinessLogicHandler getBiHandler() {
        return biHandler;
    }

}
