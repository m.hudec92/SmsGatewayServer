/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.mortbay.jetty.nio.SelectChannelConnector;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.App;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.ServerGuiOperations;

/**
 *
 * @author martinhudec
 */
public class JettyServlet implements Runnable {

    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(JettyServlet.class);
    ServerGuiOperations serverGuiOps;

    public JettyServlet(ServerGuiOperations serverGuiOps) throws Exception {
        this.serverGuiOps = serverGuiOps;
    }

    private void startServlets() throws InterruptedException, Exception {
        Server webserver = new Server(4567);
        ServerConnector connector = new ServerConnector(webserver);
        connector.setPort(4567);
        //connector.setHost("localhost");
        webserver.setConnectors(new Connector[]{connector});
        ServletContextHandler sch = new ServletContextHandler();
        sch.addServlet(new ServletHolder(new ServerApi(serverGuiOps)), "/api/*");
        webserver.setHandler(sch);
        webserver.start();
        webserver.join();
    }

    @Override
    public void run() {
        try {
            startServlets();
        } catch (Exception ex) {
            LOG.fatal("Unable to start SMS gateway server, cannot start API", ex);
            System.exit(1);
        }
    }

}
