/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects;

import com.google.gson.annotations.Expose;

/**
 *
 * @author martinhudec
 */
public class GenerateCustomAlphabet {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GenerateCustomAlphabet.class);
    @Expose
    private String userEmail;
    @Expose
    private String dataSet;
    @Expose
    private Long deviceId;

    public GenerateCustomAlphabet() {
    }

    public GenerateCustomAlphabet(String userEmail, String dataSet, Long deviceId) {
        this.userEmail = userEmail;
        this.dataSet = dataSet;
        this.deviceId = deviceId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDataSet() {
        return dataSet;
    }

    public void setDataSet(String dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public String toString() {
        return "CreateUser [userEmail=" + userEmail
                + ", dataSet=" + dataSet
                + ", deviceId=" + deviceId + "]";
    }

}
