/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding;

import com.google.api.services.gmail.model.Message;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoDataFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoDataTypeFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoDestinationNumbersFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.NoIdentificationkeyFoundInEmailException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class DeliverEmailMessage {

    private String sourceMailAddress;
    private String subject;
    private String data;
    private String emailId;
    private Message message;
    private EmailDataType dataType;
    private List<String> destinationNumberList;
    private String payload;
    private String emailIdentificationKey;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DeliverEmailMessage.class);

    public DeliverEmailMessage(String sourceMailAddress, String subject, String data, String emailId, Message message) throws NoDestinationNumbersFoundInEmailException, NoDataFoundInEmailException, NoDataTypeFoundInEmailException, NoIdentificationkeyFoundInEmailException {
        this.sourceMailAddress = sourceMailAddress;
        this.subject = subject;
        this.data = data;
        this.emailId = emailId;
        this.message = message;
        parseReceivedEmailMessage();
    }

    public String getSourceMailAddress() {
        return sourceMailAddress;
    }

    public void setSourceMailAddress(String sourceMailAddress) {
        this.sourceMailAddress = sourceMailAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public EmailDataType getDataType() {
        return dataType;
    }

    public List<String> getDestinationNumberList() {
        return destinationNumberList;
    }

    public String getPayload() {
        return payload;
    }

    public String getEmailIdentificationKey() {
        return emailIdentificationKey;
    }

    private void parseReceivedEmailMessage() throws NoDestinationNumbersFoundInEmailException, NoDataFoundInEmailException, NoDataTypeFoundInEmailException, NoIdentificationkeyFoundInEmailException {

        parseReceivedEmailMessagePlain();
    }

    private void parseReceivedEmailMessagePlain() throws NoDestinationNumbersFoundInEmailException, NoDataFoundInEmailException, NoIdentificationkeyFoundInEmailException {
        String destinationNumbersRegex = null;
        destinationNumberList = new ArrayList<>();
        try {
            logger.debug(data);
            destinationNumbersRegex = Utils.getRegexValue(data, "(?s)(?<=<--DESTINATION-->\\s).*?(?=\\s*<--DESTINATION-->)").trim();
        } catch (Utils.UnableToFindValueWithRegexException ex) {
            logger.error("Unable to find destination for e-mail message", ex);
            throw new NoDestinationNumbersFoundInEmailException("No destination numbers were found in email", ex);
        }
        if (destinationNumbersRegex.isEmpty()) {
            throw new NoDestinationNumbersFoundInEmailException("No destination numbers were found in email");
        }
        logger.debug("Destination numbers parsed from mail " + destinationNumbersRegex);
        String[] destinationNumbers = destinationNumbersRegex.split(",");
        destinationNumberList.addAll(Arrays.asList(destinationNumbers));

        String dataRegex = null;
        try {
            dataRegex = Utils.getRegexValue(data, "(?s)(?<=<--DATA-->\\s).*?(?=\\s*<--DATA-->)").trim();
        } catch (Utils.UnableToFindValueWithRegexException ex) {
            logger.error("Unable to find destination for e-mail message", ex);
            throw new NoDataFoundInEmailException("No data were found in email, no reason of processing", ex);
        }
        if (dataRegex.isEmpty()) {
            throw new NoDataFoundInEmailException("No data were found in email, no reason of processing");
        }
        payload = dataRegex;

        String keyRegex = null;
        try {
            keyRegex = Utils.getRegexValue(data, "(?s)(?<=<--IDENTIFICATION-->\\s).*?(?=\\s*<--IDENTIFICATION-->)").trim();

        } catch (Utils.UnableToFindValueWithRegexException ex) {
            logger.error("Unable to find email identificator for e-mail message", ex);
            throw new NoIdentificationkeyFoundInEmailException("No email identification were found in email, no reason of processing potencial harmful email", ex);
        }
        emailIdentificationKey = keyRegex;

    }

    public enum EmailDataType {
        PLAIN, JSON
    }

    @Override
    public String toString() {
        return "[RECEIVED EMAIL] sourceAddress " + sourceMailAddress + " emailID " + emailId + " identificationKey " + emailIdentificationKey;
    }

}
