/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.smshandler;

import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliveryStatusSmsPDU;


/**
 *
 * @author martinhudec
 */
public interface DeliveryStatusSecureMessageCallback {

    public void messageDeliveryStatusReceived(DeliveryStatusSmsPDU receivedMessage, String emailUuid, String wsUuid);

}
