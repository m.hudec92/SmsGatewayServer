/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.loadbalancer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.bouncycastle.asn1.isismtt.x509.Restriction;
import org.geotools.coverage.processing.operation.Resample;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToRetrieveDataFromDb;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.GatewayModems;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.MobileNumbers;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;

/**
 *
 * @author martinhudec
 */
public class LoadBalancer implements LoadBalancerInterface {

    private final List<ModemInstance> modemInstanceList;
    private final DbHandler dbHandler = DbHandler.getInstance();
    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(LoadBalancer.class);

    public LoadBalancer(List<ModemInstance> modemInstanceList) {
        this.modemInstanceList = modemInstanceList;
    }

    @Override
    public ModemInstance getAppropriateModem(SubmitSmsPDU submitPdu) {
        if (modemInstanceList.size() == 1) {
            return modemInstanceList.get(0);
        }
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();

        MobileNumbers number = null;
        GatewayModems modem = null;
        try {
            number = dbHandler.dbReadMobileNumber(submitPdu.getDestinationNumber(), session);
            modem = (GatewayModems) session.createCriteria(GatewayModems.class).addOrder(Order.asc("hitCount")).list().get(0);
            LOG.debug("modem " + modem.getSerialPort());
        } catch (UnableToRetrieveDataFromDb ex) {
            LOG.error("Error while reading mobile number data from DB", ex);
        }

        ModemInstance optimalModem = null;
        ModemInstance lazyModem = null;
        for (ModemInstance modemInstance : modemInstanceList) {
            LOG.debug("modem send queue size " + " serial num  " + modemInstance.getModemSerialPort() + " " + modemInstance.getMessagesToBeSentQueue().size());
            if (number != null) {
                if (modemInstance.getMobileOperator().equals(number.getClassOperatorType().getValue())) {
                    LOG.debug("optimal modem " + modemInstance.getModemSerialPort());
                    optimalModem = modemInstance;
                }
            }
            if (modem != null) {
                if (modemInstance.getModemSerialPort().equals(modem.getSerialPort())) {
                    lazyModem = modemInstance;
                }
            }
        }
        Collections.sort(modemInstanceList, (a, b) -> a.getHitCount().compareTo(b.getHitCount()));
        modemInstanceList.get(0).addModemMessageSentStat();
        if (optimalModem != null) {
            if (!optimalModem.getMobileOperator().equals(modemInstanceList.get(0).getMobileOperator())) {
                return optimalModem;
            }
        }
        
        modemInstanceList.get(0).addModemMessageSentStat();
        return modemInstanceList.get(0);
    }

}
