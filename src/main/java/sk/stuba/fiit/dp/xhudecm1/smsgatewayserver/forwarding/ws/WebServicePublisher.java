/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

/**
 *
 * @author martinhudec
 */
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.ws.Endpoint;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;

public class WebServicePublisher {

    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(WebServicePublisher.class);
    String wsdl = "empty";
    public WebServicePublisher(BusinessLogicHandler biHandler) {
        try {
            InetAddress IP = InetAddress.getLocalHost();
            String wsUrl = "http://192.168.21.3:8888/smsgatewayServer/smsWs";
            wsdl = wsUrl + "?wsdl";
            Endpoint.publish(wsUrl, new WebServiceImpl(biHandler));
            LOG.info("publishing wsdl to " + wsdl);
        } catch (UnknownHostException ex) {
            LOG.error("Unable to get local IP address error creating WS PUBLISHER", ex);
        }
    }

    public String getWsdl() {
        return wsdl;
    }

}
