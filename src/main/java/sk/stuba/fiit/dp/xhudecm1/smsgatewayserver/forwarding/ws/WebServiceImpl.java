/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

/**
 *
 * @author martinhudec
 */
import com.sun.net.httpserver.HttpExchange;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.ErrorProcessingNewWs;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.WsNotFoundInInputForwardMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsForwardInSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.MessageSentResultCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;

@WebService(endpointInterface = "sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws.WebServiceInterface")
public class WebServiceImpl implements WebServiceInterface {

    private final DbHandler dbHandler = DbHandler.getInstance();
    private final BusinessLogicHandler biHandler;
    private final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(WebServiceImpl.class);

    public WebServiceImpl(BusinessLogicHandler biHandler) {
        this.biHandler = biHandler;
    }

    @Resource
    WebServiceContext wsContext;

    @Override
    public WebServiceSendResult sendSecureSmsMessage(String destination, String data) {
        MessageContext msgContext = wsContext.getMessageContext();
        HttpExchange exchange = (HttpExchange) msgContext.get("com.sun.xml.internal.ws.http.exchange");

        Map http_headers = (Map) msgContext.get(MessageContext.HTTP_REQUEST_HEADERS);
        List authList = (List) http_headers.get("Authorization");
        String sourceIpAddress = exchange.getRemoteAddress().getAddress().getHostAddress().replace("\\/", "");
        String auth = "";
        String authValue[] = null;
        String password = null;
        String name = null;
        
        if (authList != null) {
            LOG.debug("auth " + authList.size() + " " + authList);
            auth = authList.get(0).toString();
            auth = auth.replaceAll("Basic ", "");
            authValue = new String(Base64.getDecoder().decode(auth)).split(":");
            LOG.debug("authval " + authValue);
            name = authValue[0];
            password = authValue[1];
        }
        LOG.debug("received message " + sourceIpAddress + " password " + password);
        Session session = dbHandler.getNewSession();
        Transaction tx = session.beginTransaction();
        WsReceived wsReceived = null;
        try {

            wsReceived = new WsReceived(
                    null,
                    sourceIpAddress,
                    data,
                    new Date(),
                    UUID.randomUUID().toString(),
                    destination,
                    WsHandler.WsReceivedStatusEnum.WS_RECEIVED.toString(), null, null);
            session.save(wsReceived);
            tx.commit();
            tx = session.beginTransaction();
            List<WsForwardInSettings> forwardingList = session.createCriteria(WsForwardInSettings.class).add(Restrictions.eq("wsAuthName", name)).add(Restrictions.eq("wsAuthPassword", password)).add(Restrictions.eq("sourceIpAddress", sourceIpAddress)).add(Restrictions.eq("activeStatus", "ACTIVE")).list();
            if (forwardingList.isEmpty()) {
                
                wsReceived.setWsReceivedStatusType(WsHandler.WsReceivedStatusEnum.WS_NOT_AUTHORIZED.toString());
                session.save(wsReceived);
                tx.commit();
                return new WebServiceSendResult("", "SmsGatewayServer unable to validate user");

            }
            wsReceived.setWsForwardInSettings(forwardingList.get(0));
            session.save(wsReceived);
            tx.commit();
            biHandler.processReceivedWsMessage(new DeliverWsMessage(
                    sourceIpAddress,
                    data,
                    destination,
                    name,
                    password,
                    wsReceived.getWsMessageUuid(),
                    forwardingList.get(0).getId(),
                    wsReceived.getWsMessageUuid()));
            
            return new WebServiceSendResult(wsReceived.getWsMessageUuid(), "SUCCESS");
        } catch (HibernateException ex) {
            tx.rollback();
            LOG.debug("Error ", ex);
        } catch (ErrorProcessingNewWs | WsNotFoundInInputForwardMappings ex) {
            LOG.debug("Error ", ex);
        } finally {
            session.close();
        }
        return new WebServiceSendResult("", "ERROR_PROCESSING_WS");
    }
}
