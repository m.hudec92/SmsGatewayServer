/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions;

/**
 *
 * @author martinhudec
 */
public class EmailNotFoundInInputForwardMappings extends Exception {

    public EmailNotFoundInInputForwardMappings(String string) {
        super(string);
    }

    public EmailNotFoundInInputForwardMappings(String string, Throwable th) {
        super(string, th);
    }
}
