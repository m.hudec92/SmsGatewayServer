/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.jsonobjects;

import com.google.gson.annotations.Expose;

/**
 *
 * @author martinhudec
 */
public class RegisterNewDevice {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RegisterNewDevice.class);
    @Expose
    private String userEmail;
    @Expose
    private String deviceName;
    @Expose
    private String description;
    @Expose
    private Long mobileNumberId;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMobileNumber() {
        return mobileNumberId;
    }

    public void setMobileNumber(Long mobileNumberId) {
        this.mobileNumberId = mobileNumberId;
    }
    
  
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        return "CreateUser [userEmail=" + userEmail 
                + ", mobileNumberId=" + mobileNumberId
                + ", description=" + description
                + ", deviceName=" + deviceName+ "]";
    }

}
