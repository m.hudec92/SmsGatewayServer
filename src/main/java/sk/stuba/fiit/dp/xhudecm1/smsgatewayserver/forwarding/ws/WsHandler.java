/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

import java.net.MalformedURLException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.BusinessLogicHandler;
import java.net.URL;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import org.hibernate.Session;
import org.hibernate.Transaction;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.ErrorSendingWsMessageException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.UnableToRetrieveDataFromDb;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.BiMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.ClientSecureCommunicationData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.SmsReceived;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsForwardOutSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.WsSent;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliveryStatusSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;

/**
 *
 * @author martinhudec
 */
public class WsHandler {

    private BusinessLogicHandler biHandler;
    private final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(WsHandler.class);
    private DbHandler dbHandler;
    private WebServicePublisher publisher;

    public WsHandler(BusinessLogicHandler biHandler) {
        this.biHandler = biHandler;
        this.dbHandler = DbHandler.getInstance();
        publisher = new WebServicePublisher(biHandler);
    }

    public void forwardMessageViaWs(DeliverSmsPDU receivedSmsPdu, Set<WsForwardOutSettings> wsForwardOutSettings) throws UknownPduConentType {

        for (WsForwardOutSettings wsForwardSettings : wsForwardOutSettings) {
            
            if (wsForwardSettings.getActiveStatus().equals("ACTIVE")) {
                WsSent sentWs = null;
                try {
                    SmsReceived receivedSmsDao = dbHandler.dbReadSmsReceived(receivedSmsPdu.getMessageUUID());
                    sentWs = new WsSent(receivedSmsDao,
                            wsForwardSettings,
                            new String(receivedSmsPdu.getSecurePdu().getPureData()),
                            new Date(), WsSentStatusEnum.WS_SENDING.toString(),
                            UUID.randomUUID().toString(),
                            SentWsMessageType.WS_DATA.toString());
                    dbHandler.dbWriteSentWs(sentWs);
                } catch (UnableToRetrieveDataFromDb ex) {
                    LOG.error("Error while storing sent ws in DB for " + receivedSmsPdu.toString(), ex);
                }
                try {

                    sendWsMessage(wsForwardSettings.getDestinationWsWsdlUrl(),
                            wsForwardSettings.getWsPortName(),
                            wsForwardSettings.getWsServiceName(),
                            wsForwardSettings.getWsNamespaceUri(),
                            sentWs.getWsMessageUuid(),
                            new String(receivedSmsPdu.getSecurePdu().getPureData()),
                            receivedSmsPdu.getOriginatorNumber());
                } catch (ErrorSendingWsMessageException ex) {
                    LOG.error("Error while storing sent ws in DB for " + receivedSmsPdu.toString(), ex);
                    try {
                        dbHandler.dbUpdateSentWsStatus(sentWs.getWsMessageUuid(), WsSentStatusEnum.WS_SENT_ERROR);
                    } catch (UnableToRetrieveDataFromDb ex1) {
                        LOG.error("Error while changing statusfor sent ws in DB for " + receivedSmsPdu.toString(), ex1);
                    }
                }
                try {
                    dbHandler.dbUpdateSentWsStatus(sentWs.getWsMessageUuid(), WsSentStatusEnum.WS_SENT);
                } catch (UnableToRetrieveDataFromDb ex) {
                    LOG.error("Error while changing statusfor sent ws in DB for " + receivedSmsPdu.toString(), ex);
                }
            }
        }
    }

    public void sendWsMessage(String wsdlUrl, String wsPortName, String wsServiceName, String wsNamespaceUri, String messageUuid, String data, String originatorNumber) throws ErrorSendingWsMessageException {
        try {

            URL url = new URL(wsdlUrl);
            QName qname = new QName(wsNamespaceUri, wsServiceName);
            Service service = Service.create(url, qname);
            QName qNamePort = new QName(wsNamespaceUri, wsPortName);
            ClientEndpointWebServiceInterface clientEndpoint = service.getPort(qNamePort, ClientEndpointWebServiceInterface.class);
            String response = clientEndpoint.secureSmsMessageReceived(messageUuid, originatorNumber, data);
            LOG.debug("response " + response);
        } catch (MalformedURLException ex) {
            LOG.error("Error sending WS Message ", ex);
            throw new ErrorSendingWsMessageException("Error sending WS Message", ex);
        } catch (Exception ex) {
            LOG.error("Error sending WS Message ", ex);
            throw new ErrorSendingWsMessageException("Error sending WS Message", ex);
        }
    }

    public void sendWsDeliveryStatusReportMessage(String wsdlUrl, String wsPortName, String wsServiceName, String wsNamespaceUri, String messageUuid, String deliveryStatus) throws ErrorSendingWsMessageException {
        try {

            URL url = new URL(wsdlUrl);
            QName qname = new QName(wsNamespaceUri, wsServiceName);
            Service service = Service.create(url, qname);
            QName qNamePort = new QName(wsNamespaceUri, wsPortName);
            ClientEndpointWebServiceInterface clientEndpoint = service.getPort(qNamePort, ClientEndpointWebServiceInterface.class);

            String response = clientEndpoint.sentMessageDelivered(messageUuid, deliveryStatus);
            LOG.debug("response " + response);
        } catch (MalformedURLException ex) {
            LOG.error("Error sending WS Message ", ex);
            throw new ErrorSendingWsMessageException("Error sending WS Message", ex);
        } catch (Exception ex) {
            LOG.error("Error sending WS Message ", ex);
            throw new ErrorSendingWsMessageException("Error sending WS Message", ex);
        }
    }

    public void forwardMessageDeliveryResponseViaWs(Long secureDataId, String wsUuid, DeliveryStatusSmsPDU deliveryStatusPdu) {
        Session session = dbHandler.getNewSession();
        Transaction tx = session.getTransaction();

        LOG.debug("seding ws message deliveyr response for uuid " + wsUuid);
        ClientSecureCommunicationData secureData = (ClientSecureCommunicationData) session.get(ClientSecureCommunicationData.class, secureDataId);
        for (WsForwardOutSettings forwardOut : secureData.getWsForwardOutSettingses()) {
            if (forwardOut.getActiveStatus().equals("ACTIVE")) {
                try {
                    sendWsDeliveryStatusReportMessage(forwardOut.getDestinationWsWsdlUrl(), forwardOut.getWsPortName(), forwardOut.getWsServiceName(), forwardOut.getWsNamespaceUri(), wsUuid, deliveryStatusPdu.getDeliveryStatusType().toString());
                } catch (ErrorSendingWsMessageException ex) {
                    LOG.error("Error forwarding sms Delivery status report", ex);
                }
            }
        }
        session.close();
    }

    public enum SentWsMessageType {
        WS_DATA, WS_DELIVERY_RESPONSE
    }

    public enum WsReceivedStatusEnum {
        WS_RECEIVED, WS_PROCESSING, WS_PROCESSED, WS_ERROR, WS_NOT_AUTHORIZED
    }

    public enum WsSentStatusEnum {
        WS_SENDING, WS_SENT, WS_SENT_ERROR
    }

    public String getWsdl() {
        return publisher.getWsdl();
    }
}
