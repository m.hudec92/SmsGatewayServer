/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.bi.OptionProperties;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.databaseconnector.DbHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.exceptions.OptionPropertiesException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.httpserver.JettyServlet;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.smshandler.ServerSmsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;

/**
 *
 * @author martinhudec
 */
public class App {

    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(App.class);

    /**
     * @param args the command line arguments
     */
    public static OptionProperties OPTION_PROPERTIES = null;

    public static void main(String[] args) {

        try {
            try {
                OPTION_PROPERTIES = loadOptionProperties();
            } catch (FileNotFoundException ex) {
                throw new RuntimeException("Unable to load option.properties file cannot continue", ex);
            }
            LOG.info("OPTION.PROPERTIES succesfully loaded");
            LOG.debug(OPTION_PROPERTIES.toString());

            DbHandler dbHandler = DbHandler.getInstance();

            ServerSmsHandler smsHandler = null;
            try {
                smsHandler = new ServerSmsHandler();
                JettyServlet serverApiServlet = new JettyServlet(smsHandler.getBiHandler().getServerGuiOpsHandler());
                new Thread(serverApiServlet).start();
                for (int i = 0; i < 1; i++) {
                    smsHandler.sendMessage(new SubmitSmsPDU("+421908632636", ("test " + i).getBytes()));
                }

            } catch (Exception ex) {
                LOG.fatal("UNABLE TO INITIALIZE MODEMS", ex);
            }
        } catch (RuntimeException ex) {
            LOG.fatal("UNABLE TO INITIALIZE SERVER SMS gateway", ex);
        }
    }

    private static OptionProperties loadOptionProperties() throws FileNotFoundException {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        BufferedReader reader = new BufferedReader(new FileReader("/opt/smsgatewayserver/option.properties"));
        OptionProperties data = gson.fromJson(reader, OptionProperties.class);
        return data;
    }
}
