/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

/**
 *
 * @author martinhudec
 */
public class WebServiceSendResult {
    String messageUUID; 
    String sendResult; 

    public WebServiceSendResult(String messageUUID, String sendResult) {
        this.messageUUID = messageUUID;
        this.sendResult = sendResult;
    }

    public String getMessageUUID() {
        return messageUUID;
    }

    public void setMessageUUID(String messageUUID) {
        this.messageUUID = messageUUID;
    }

    public String getSendResult() {
        return sendResult;
    }

    public void setSendResult(String sendResult) {
        this.sendResult = sendResult;
    }
    
}
