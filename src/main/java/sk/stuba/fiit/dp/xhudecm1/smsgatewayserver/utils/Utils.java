/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author martinhudec
 */
public class Utils {

    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    public static byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }

    public static String getRegexValue(String string, String regex) throws UnableToFindValueWithRegexException {
        Matcher matcher = Pattern.compile(regex).matcher(string);

        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new UnableToFindValueWithRegexException("Nothing found with", string, regex);
        }
    }

    public static class UnableToFindValueWithRegexException extends Exception {

        public UnableToFindValueWithRegexException(String message, String string, String regex) {
            super(message + " regex: " + regex + " in " + string);
        }
    }
}
