/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding.ws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author martinhudec
 */
public class DeliverWsMessage {

    private String sourceIpAddress;
    private List<String> destinationNumberList;
    private String payload;
    private String name; 
    private String password;
    private String wsIdentificationKey;
    private Long wsForwardingId;
    private String wsUuid;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DeliverWsMessage.class);

    public DeliverWsMessage(String sourceIpAddress, String payload, String destinationNumberList, String name, String password, String wsIdentificationKey, Long wsForwardingId, String wsUuid) {
        this.sourceIpAddress = sourceIpAddress;
        this.payload = payload;
        this.name = name;
        this.password = password;
        this.wsIdentificationKey = wsIdentificationKey;
        this.wsForwardingId = wsForwardingId;
        this.wsUuid = wsUuid;
        parseDestinationNumbers(destinationNumberList);

    }


    public String getSourceIpAddress() {
        return sourceIpAddress;
    }

    public void setSourceIpAddress(String sourceIpAddress) {
        this.sourceIpAddress = sourceIpAddress;
    }

    public List<String> getDestinationNumberList() {
        return destinationNumberList;
    }

    public void setDestinationNumberList(List<String> destinationNumberList) {
        this.destinationNumberList = destinationNumberList;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getWsIdentificationKey() {
        return wsIdentificationKey;
    }

    public void setWsIdentificationKey(String wsIdentificationKey) {
        this.wsIdentificationKey = wsIdentificationKey;
    }

    public Long getWsForwardingId() {
        return wsForwardingId;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getWsUuid() {
        return wsUuid;
    }

    @Override
    public String toString() {
        return "[RECEIVED WS] sourceIpAddress " + sourceIpAddress + " destinationNumberList " + destinationNumberList + " ws identification key " +wsIdentificationKey + " wsUUID " + wsUuid;
    }

    private void parseDestinationNumbers(String destinationNumbers) {
        String[] destNums = destinationNumbers.split(",");
        destinationNumberList = new ArrayList(Arrays.asList(destNums));
    }

}
