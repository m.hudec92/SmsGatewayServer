/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayserver.forwarding;

import java.io.IOException;
import java.util.Set;
import javax.mail.MessagingException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.BiMappings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailForwardInSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayserverdbmapper.EmailForwardOutSettings;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;

/**
 *
 * @author martinhudec
 */
public interface EmailHandlerInterface {

    public void sendEmail(DeliverSmsPDU receivedSmsPdu, Set<EmailForwardOutSettings> emailForwardSettings) throws UknownPduConentType;
}
